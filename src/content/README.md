# Content directory

This directory must contain the assessment's content files:

* **config.yaml**: The assessment's configuration file
* **handler.js**: User-defined code to influence the assessment's behavior
* **Assessment definition file**: A YAML file that defines the structure, pages and questions of the assessment
* **Language files**: YAML files that contain the assessment's strings; one file per language

You will find templates for all these files in /doc/templates. Sample assessments that demonstrate the features
of the assessment can be found in /doc/samples.

## Important

This directory contains a .gitignore file that makes Git ignore all files
except this README and the .gitignore file itself.

**If you want to track your content files in a Git repository of your own, you
should update or remove the .gitignore file.**
