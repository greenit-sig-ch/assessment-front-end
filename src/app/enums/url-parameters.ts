/**
 * Known URL parameters.
 * - {@link lang} - current language used (example: http://site.com?lang=de).
 * - {@link test}.
 */
export enum URL_PARAMETERS {
	lang,
	test
}
