import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { COMMON_LANG, HttpService, IInitializationData, YAML } from './service/http.service';

import { first } from 'rxjs/operators';

import { ConfigService } from './service/config.service';
import { DataService } from './service/data.service';
import { LoggerService, MODE } from './service/logger.service';

import { URL_PARAMETERS } from './enums/url-parameters';
import { Configuration } from './vo/configuration';

/**
 * Model of language files to load.
 */
interface ILangItemToLoad {
	locale: string;
	files: Array<string>;
}

/**
 * Main application component.
 *
 * @export
 * @class AppComponent
 * @implements {OnInit}
 */
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of AppComponent.
	 *
	 * @param logger
	 * @param httpService
	 * @param dataService
	 * @param configService
	 * @param title
	 */
	constructor(
		private readonly logger: LoggerService,
		private readonly httpService: HttpService,
		private readonly dataService: DataService,
		private readonly configService: ConfigService,
		private readonly title: Title,
	) {}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Init application.
	 * This is main entry point.
	 *
	 * @returns {void}
	 */
	private initApp(): void {
		this.logger.log(MODE.log, 'Init config. Current user locale:', this.configService.environment.locale);

		// load configuration and questionnaire
		this.httpService.loadConfigurationAndQuestionnaire()
			.pipe(first())
			.subscribe((result: IInitializationData) => {
				this.logger.log(MODE.log, 'Configuration and script OK:', result);

				const configuration: Configuration = result.configuration;
				configuration.urlParameters = this.configService.parseURLParameters();
				configuration.langPackHashMap = new Map<string, string>();

				// make possible locales list
				const longLangLocale: string = this.configService.environment.locale;
				const shortLangLocale: string = longLangLocale.split('-')[0];
				const localeAliasList: Array<string> = [
					longLangLocale,
					shortLangLocale,
					configuration.scriptLanguage
				];

				// check URL for language link like a "http://domain.com/test/just/XX/for/example"
				const href = document.location.href;
				if (href) {
					const matches = href.match(/\/[a-z]{2}\//);
					const urlLang = matches && matches.length ? matches[0].replace(/\//g, '') : undefined;
					if (urlLang && urlLang in configuration.languages) {
						localeAliasList.unshift(urlLang);
					}
				}

				// check URL "lang" parameter like a "http://domain.com/test?lang=XX"
				if (configuration.urlParameters.has(URL_PARAMETERS[URL_PARAMETERS.lang])) {
					const ul: string = configuration.urlParameters.get(URL_PARAMETERS[URL_PARAMETERS.lang]);
					localeAliasList.unshift(ul);
				}

				// create array of possible lang items to load
				const loadList: Array<ILangItemToLoad> = localeAliasList
					.map(m => {
						let files: Array<string>;
						if (configuration.languages[m]) {
							files = Array.isArray(configuration.languages[m])
								? configuration.languages[m]
								: [configuration.languages[m]];
						}

						return {
							locale: m,
							files
						};
					})
					.filter(p => p.files);

				// if array isn't empty we can request prepared items
				if (loadList.length > 0) {
					const firstItemToLoad = loadList[0];
					const commonLang: string = COMMON_LANG + firstItemToLoad.locale + YAML;

					this.httpService.loadLanguageFiles([...firstItemToLoad.files, commonLang])
						.subscribe((langPackHashMap: Map<string, string>) => {
							// configuration.langPackHashMap = langPackHashMap;

							// update window title if resource is present
							if (langPackHashMap.has('app-title')) {
								this.title.setTitle(langPackHashMap.get('app-title'));
							}

							// init app and start
							this.configService.setConfiguration({
								...configuration,
								langPackHashMap,
								scriptLanguage: firstItemToLoad.locale,
							});
							this.dataService.setQuestionnaire(result);
						}, errorLang => {
							this.logger.log(MODE.error, 'Check configuration of languages files!', errorLang);
						});
				}
			}, error => {
				this.logger.log(MODE.error, 'Check configuration file!', error);
			});
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
		this.initApp();
	}
}
