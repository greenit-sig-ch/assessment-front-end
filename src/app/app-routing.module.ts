import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionnaireComponent } from './view/questionnaire/questionnaire.component';

const routes: Routes = [
	// {path: '', redirectTo: '/page', pathMatch: 'full'},
	// {path: 'page/print', component: PrintPageComponent},
	// {path: 'page/:id', component: QuestionnaireComponent},
	// {path: 'page', component: QuestionnaireComponent},
	{path: '**', component: QuestionnaireComponent, children: []}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
	exports: [RouterModule]
})
export class AppRoutingModule {}
