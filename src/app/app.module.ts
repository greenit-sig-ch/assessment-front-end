import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { NgxMdModule } from 'ngx-md';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppRoutingModule } from './app-routing.module';
import { LocalizationPipe } from './pipe/localization.pipe';

import { ConfigService } from './service/config.service';
import { DataService } from './service/data.service';
import { HttpService } from './service/http.service';
import { LoggerService } from './service/logger.service';
import { RestApiService } from './service/rest-api.service';

import { AppComponent } from './app.component';
import { HorizontalBarComponent } from './view/horizontal-bar/horizontal-bar.component';
import { PageNavigatorComponent } from './view/navigator/page-navigator.component';
import { PageTypeEvaluationComponent } from './view/pages/page-type-evaluation/page-type-evaluation.component';
import { PageImportModalAlertComponent } from './view/pages/page-type-export/page-import-modal-alert/page-import-modal-alert.component';
import { PageTypeExportComponent } from './view/pages/page-type-export/page-type-export.component';
import { PageTypeQuestionComponent } from './view/pages/page-type-question/page-type-question.component';
import { PageComponent } from './view/pages/page.component';
import { PageTitleComponent } from './view/pages/title/page-title.component';
import { PrintPageComponent } from './view/print-page/print-page.component';
import { QuestionnaireComponent } from './view/questionnaire/questionnaire.component';
import { BasicQuestionTypeComponent } from './view/questions/basic-question-type.component';
import { QuestionDescriptionComponent } from './view/questions/description/question-description.component';
import { DropDownComponent } from './view/questions/drop-down/drop-down.component';
import { MultiTextFieldComponent } from './view/questions/multi-text-field/multi-text-field.component';
import { MultipleChoiceComponent } from './view/questions/multiple-choice/multiple-choice.component';
import { QuestionComponent } from './view/questions/question.component';
import { SingleChoiceComponent } from './view/questions/single-choice/single-choice.component';
import { TextAreaComponent } from './view/questions/text-area/text-area.component';
import { TextFieldComponent } from './view/questions/text-field/text-field.component';
import { ResetModalAlertComponent } from './view/reset-modal-alert/reset-modal-alert.component';

/**
 * Main init factory function.
 *
 * @param {ConfigService} configService
 * @returns {Function}
 */
export const startupServiceFactory = (configService: ConfigService): Function => () => {
	configService.initConfiguration();
};

/**
 * Main application module.
 *
 * @export
 * @class AppModule
 */
@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpClientModule,
		AppRoutingModule,

		LayoutModule,
		ModalModule.forRoot(),
		NgxMdModule.forRoot(),
		ToastrModule.forRoot({
			timeOut: 10000,
			positionClass: 'toast-top-right',
			preventDuplicates: true,
			closeButton: true,
		}),
	],
	declarations: [
		AppComponent,
		LocalizationPipe,
		QuestionnaireComponent,
		PageComponent,
		PageNavigatorComponent,
		PageTypeQuestionComponent,
		PageTypeEvaluationComponent,
		PageTypeExportComponent,
		PageImportModalAlertComponent,
		PageTitleComponent,
		QuestionComponent,
		BasicQuestionTypeComponent,
		QuestionDescriptionComponent,
		TextAreaComponent,
		TextFieldComponent,
		SingleChoiceComponent,
		MultipleChoiceComponent,
		HorizontalBarComponent,
		ResetModalAlertComponent,
		PrintPageComponent,
		DropDownComponent,
		MultiTextFieldComponent
	],
	providers: [
		HttpService,
		DataService,
		RestApiService,
		LoggerService,
		LocalizationPipe,
		ConfigService,
		{
			provide: APP_INITIALIZER,
			useFactory: startupServiceFactory,
			deps: [ConfigService],
			multi: true
		}
	],
	entryComponents: [ResetModalAlertComponent],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {}
