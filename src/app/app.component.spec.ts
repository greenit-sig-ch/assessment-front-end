import { TestBed, waitForAsync } from '@angular/core/testing';

import {HttpService} from './service/http.service';
import {DataService} from './service/data.service';

import {LocalizationPipe} from './pipe/localization.pipe';

import {AppComponent} from './app.component';
import {PageComponent} from './view/pages/page.component';
import {PageTitleComponent} from './view/pages/title/page-title.component';
import {PageNavigatorComponent} from './view/navigator/page-navigator.component';
import {QuestionComponent} from './view/questions/question.component';
import {QuestionDescriptionComponent} from './view/questions/description/question-description.component';
import {TextAreaComponent} from './view/questions/text-area/text-area.component';

describe('AppComponent', () => {
	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent,
				PageComponent,
				PageTitleComponent,
				PageNavigatorComponent,
				QuestionComponent,
				QuestionDescriptionComponent,
				TextAreaComponent,

				LocalizationPipe
			],
			providers: [HttpService,DataService]
		}).compileComponents();
	}));

// 	it('should create the app', async(() => {
// 		const fixture = TestBed.createComponent(AppComponent);
// 		const app = fixture.debugElement.componentInstance;
// 		expect(app).toBeTruthy();
// 	}));

// 	it(`should have as title 'app works!'`, async(() => {
// 		const fixture = TestBed.createComponent(AppComponent);
// 		const app = fixture.debugElement.componentInstance;
// 		expect(app.title).toEqual('app works!');
// 	}));

// 	it('should render title in a h1 tag', async(() => {
// 		const fixture = TestBed.createComponent(AppComponent);
// 		fixture.detectChanges();
// 		const compiled = fixture.debugElement.nativeElement;
// 		expect(compiled.querySelector('h1').textContent).toContain('app works!');
// 	}));
});
