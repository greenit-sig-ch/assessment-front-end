import { MODE } from '../service/logger.service';

// /**
// * Tracking mode.
// * <ul>
// *    <li>0 - No tracking</li>
// *    <li>1 - Only track page open events</li>
// *    <li>2 - Track which questions have been answered but not the answer itself</li>
// *    <li>3 - Track which questions have been answered and also transmit the answer</li>
// *    <li>4 - Track all user activity</li>
// * </ul>
// */
// export enum TrackingMode {
// 	NO_TRACKING,					// 0
// 	PAGE_TRACKING,					// 1
// 	QUESTION_TRACKING,				// 2
// 	QUESTION_WITH_ANSWER_TRACKING,	// 3
// 	TRACK_ALL						// 4
// }

/**
 * Application configuration model.
 */
export class Configuration {

	version = 1;

	/**
	 * Path to the script file.
	 */
	scriptFile: string;

	/**
	 * Default language used for UI.
	 */
	scriptLanguage: string;

	/**
	 * On/off page navigator.
	 */
	showPageNavigator = true;

	/**
	 * Language file object.
	 */
	languages: any;

	/**
	 * Show page number on the page title.
	 */
	showPageNumber = true;

	/**
	 * Show question number on the question title.
	 */
	showQuestionNumber = true;

	/**
	 * Undefined strings control.
	 * If true will show undefined string like a %key% otherwise will show empty string.
	 */
	showUndefinedStrings = false;

	/**
	 * Show descriptions immediately. Hide toggle button if <b>true</b>.
	 */
	showDescriptionsImmediately = false;

	// /**
	//  * Tracking mode.
	//  */
	// public trackingMode: TrackingMode = 0;

	/**
	 * If true, track page open events.
	 */
	trackPageOpen: boolean;

	/**
	 * If true, track which questions have been answered but not the answer.
	 */
	trackQuestions: boolean;

	/**
	 * If true, track the questions with answers.
	 * If the user specifies trackQuestions: false and trackAnswers: true, then trackAnswers wins.
	 */
	trackAnswers: boolean;

	/**
	 * Rest API URL.
	 */
	restApiUrl = '';

	/**
	 * Logging level.
	 */
	logLevel: MODE = MODE.off;

	/**
	 * Action application host URL.
	 * Can be a string or an object.
	 * - Old way:
	 * actionAppHost: "https://greenit-switzerland.ch/app/mc-dc"
	 *
	 * - New way:
	 * actionAppHost:
	 *   de: "https://greenit-switzerland.ch/rz-massnahmen"
	 *   en: "https://greenit-switzerland.ch/dc-measures"
	 *   fr: "https://greenit-switzerland.ch/cc-mesures"
	 */
	actionAppHost: string | { [key: string]: string };

	/**
	 * The URL parameters specified during application startup.
	 */
	urlParameters: Map<string, string>;

	// -----------------------------
	//  Calculated properties
	// -----------------------------

	langPackHashMap: Map<string, string>;
}
