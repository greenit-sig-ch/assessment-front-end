import { Node } from './node';
import { Page } from './page';
import { Question } from './question';

/**
 * The questionnaire is the top level object for the definition of assessments.
 * Contain arrays of page, questions, page structure and other properties.
 */
export class Questionnaire {
	/**
	 * Version number.
	 *
	 * @type {number}
	 */
	version: number;

	/**
	 * The questionnaire's (human readable) identification string.
	 *
	 * @type {string}
	 */
	id: string;

	/**
	 * The questionnaire's title.
	 *
	 * @type {string}
	 */
	title: string;

	/**
	 * Collection of Question objects.
	 *
	 * @type {Array<Question>}
	 */
	questions: Array<Question>;

	/**
	 * Collection of Page objects.
	 *
	 * @type {Array<Page>}
	 */
	pages: Array<Page>;

	/**
	 * Collection of Node objects.
	 *
	 * @type {Array<Node>}
	 */
	structure: Array<Node>;
}
