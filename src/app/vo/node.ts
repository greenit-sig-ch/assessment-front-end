/**
 * Application structure model.
 * Node defines the hierarchy of pages in the questionnaire.
 * Any number of nesting is allowed.
 */
export class Node {
	/**
	 * Reference to page by Id.
	 */
	rid = '';

	/**
	 * This page's weight during evaluation.
	 */
	weight = 0;

	/**
	 * If <b>true</b> then the page is displayed.
	 * If <b>false</b>, then the page and all its child pages are not displayed.
	 * Default value: <b>true</b>.
	 */
	enabled = true;

	/**
	 * Array of inner items like a Node (children pages).
	 */
	children: Array<Node>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of Node.
	 */
	constructor() {
		this.children = new Array<Node>();
	}

}
