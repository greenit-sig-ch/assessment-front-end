/**
 * Model of option for the question.
 * Actual only for <b>singleChoice</b> and <b>multipleChoice</b> questions.
 */
export class Option {
	/**
	 * The option's (human readable) identification string.
	 * Must be unique within one question but does not have to be unique over the complete questionnaire.
	 */
	id: string;

	/**
	 * The score that is assigned to the question if the user selects this option.
	 * Should not be higher than the value provided by the question's maxScore property.
	 */
	score = 0;

	/**
	 * The answer that is displayed.
	 * <b>Is localizable</b>.
	 */
	title: string;

	/**
	 * Empty or one of <b>OptionType</b> values:
	 * <ul>
	 *    <li><b>dnk</b> – do not know.</li>
	 *    <li><b>nr</b> – not relevant.</li>
	 * </ul>
	 * @class OptionType
	 */
	type = '';
}

/**
 * Set of option type.
 */
export enum OptionType {
	/**
	 * <b>Do not know</b> options do not have a special meaning yet.
	 */
	'dnk',

	/**
	 * <b>Not relevant</b>.<br>
	 * Questions for which the user selects the <b>Not relevant</b> option
	 * are not considered when computing the user’s score for the page
	 * on which the question resides.
	 */
	'nr'
}
