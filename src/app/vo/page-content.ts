/**
 * Page content model.
 * Used as array item for <b>contents</b> property of <b>Page</b> model.
 *
 * @export
 * @class PageContent
 */
export class PageContent {
	/**
	 * Reference to question by Id.
	 *
	 * @type {string}
	 */
	public rid: string;

	/**
	 * If <b>true</b> then the question is displayed.
	 * Default value: <b>true</b>.
	 *
	 * @type {boolean}
	 */
	public enabled: boolean = true;

	/**
	 * This question's weight during evaluation.
	 *
	 * @type {number}
	 */
	public weight: number = 1;

	/**
	 * If <b>true</b> then an answer has to be given to this question
	 * before the user can navigate to the next page.
	 * Default value: <b>false</b>.
	 *
	 * @type {boolean}
	 */
	public mandatory: boolean = false;
}
