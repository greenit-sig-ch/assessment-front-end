import { Answer } from './answer';
import { Questionnaire } from './questionnaire';

/**
 * Model for save answers to local storage.
 */
export class SaveAnswer {
	/**
	 * Questionnaire ID.
	 */
	qid: string;

	/**
	 * Questionnaire version.
	 */
	version: number;

	/**
	 * Questionnaire md5 file hash.
	 */
	hash: string;

	/**
	 * UUID.
	 */
	uuid: string;

	/**
	 * Answers for questionnaire.
	 */
	answers: Array<any>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(questionnaire: Questionnaire, hash: string, uuid: string) {
		// convert answers
		const answers: Array<any> = new Array<Answer>();
		let ao: any;
		questionnaire.questions.forEach(question => {
			ao = Answer.toSimpleObject(question.answer);
			answers.push(ao);
		});

		this.qid = questionnaire.id;
		this.version = questionnaire.version;
		this.hash = hash;
		this.uuid = uuid;
		this.answers = answers;
	}
}
