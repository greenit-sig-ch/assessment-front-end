/**
 * Model of tracking request.
 */
export class Tracking {
	/**
	 * Logging event type (EVENT_TYPE_PAGE_OPENED, EVENT_TYPE_ANSWER_GIVEN).
	 */
	eventType: string;

	uuid: string;

	assessmentId: string;

	assessmentVersion: number;

	pageId: string;

	pageVersion: number;

	questionId: string;

	questionVersion: number;

	optionId: string;

	optionText: string;

	optionComment: string;

	language: string;
}
