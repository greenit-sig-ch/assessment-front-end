/**
 * Answer model.
 *
 * @export
 * @class Answer
 */
export class Answer {

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Reference to question by Id.
	 */
	qid: string;

	/**
	 * Map of selected option item.
	 * As key is used option ID.
	 */
	selection: Map<string, boolean>;

	/**
	 * Comment for answer.
	 */
	comment: string;

	/**
	 * Answer text value for question types like a <b>textArea</b> or <b>textField</b>.
	 */
	text: string;

	/**
	 * Object with multitext content.
	 *
	 * @type {any}
	 */
	multiFieldTexts: any = {};

	// -----------------------------
	//  Static functions
	// -----------------------------

	/**
	 * Convert Answer model to simple object.
	 *
	 * @param {Answer} answer
	 * @return {Object}
	 */
	static toSimpleObject(answer: Answer): Object {
		const selection: any = {};
		answer.selection.forEach((v: boolean, k: string) => selection[k] = v);

		return {
			qid: answer.qid,
			comment: answer.comment,
			text: answer.text,
			selection,
			multiFieldTexts: answer.multiFieldTexts
		};
	}

	/**
	 * Convert simple object to Answer model.
	 *
	 * @param {any} simpleObject
	 * @return {Answer}
	 */
	static fromSimpleObject(simpleObject: any): Answer {
		const answer: Answer = new Answer();

		answer.qid = simpleObject['qid'];
		answer.comment = simpleObject['comment'];
		answer.text = simpleObject['text'];

		const selectionMap: Map<string, boolean> = new Map<string, boolean>();
		Object.keys(simpleObject['selection'])
			.forEach(key => selectionMap.set(key, simpleObject['selection'][key]));

		answer.selection = selectionMap;

		if (simpleObject['multiFieldTexts']) {
			answer.multiFieldTexts =  simpleObject['multiFieldTexts'];
		}

		return answer;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of Answer.
	 */
	constructor() {
		this.qid = '';
		this.selection = null;
		this.comment = '';
		this.text = '';
	}
}
