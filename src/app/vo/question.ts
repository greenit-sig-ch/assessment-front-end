import { Answer } from './answer';
import { Option } from './option';

/**
 * Question types enumeration.
 *
 * @export
 * @enum {number}
 */
export enum QuestionType {
	/**
	 * The user can select zero, one or more of multiple options.
	 */
	multipleChoice,

	/**
	 * The user can select one of multiple options.
	 */
	singleChoice,

	/**
	 * The user can enter text spanning multiple lines.
	 */
	textArea,

	/**
	 * The user can enter text on one line.
	 */
	textField,

	/**
	 * Questions of type dropDown are similar to questions of type singleChoice,
	 * but instead of a list of radio buttons, a drop-down field is displayed.
	 */
	dropDown,

	/**
	 * This question type will display more than one text field field in one row.
	 */
	multiTextField
}

/**
 * Application question model.<br>
 * Questions for which the user selects the “not relevant” option are not considered
 * when computing the user’s score for the page on which the question resides.
 * “Do not know” options do not have a special meaning yet.
 *
 * @export
 * @class Question
 */
export class Question {
	/**
	 * Version number.
	 */
	version: number;

	/**
	 * The question's (human readable) identification string.
	 * It is the responsibility of the person defining an assessment to make sure
	 * that this id is unique within the assessment.
	 */
	id: string;

	/**
	 * The question displayed to the user.
	 * <b>Is localizable</b>.
	 */
	title: string;

	/**
	 * Description, additional information to clarify the question.
	 * <b>Is localizable</b>.
	 */
	description: string;

	/**
	 * The id of a string resource that may be displayed beneath the options
	 * of a question as an immediate feedback to the user after the user has selected an answer.
	 * This id may be set in an onAnswerGiven plugin method.
	 * Default value: "".
	 */
	feedbackId = '';

	/**
	 * Question type.
	 * Contains string equivalent of value from enumeration <b>QuestionType</b>.
	 *
	 * @see {QuestionType}
	 */
	type: string;

	/**
	 * If <b>true</b> then a field to enter comments is displayed below the answer fields.
	 * Default value: <b>false</b>.
	 */
	allowComments = false;

	/**
	 * If true then the question has a score that has to be taken into account during evaluation.
	 * Default value: <b>true</b> for <b>singleChoice</b> questions and <b>false</b> otherwise.
	 */
	hasScore: boolean;

	/**
	 * The maximum score that the user can get when answering this question.
	 */
	maxScore: number;

	/**
	 * An array with the id's of actions that are relevant to this question.
	 */
	actionIds: Array<string>;

	/**
	 * Name of a function that is called when the user has selected an option
	 * in singleChoice or multipleChoice questions.
	 * Default value: "".
	 */
	onAnswerGiven = '';

	/**
	 * An array with Option objects; only for singleChoice and multipleChoice questions.
	 */
	options: Array<Option>;

	/**
	 * Name of a function that is called to calculate this question's score.
	 *
	 * @type {string}
	 * @default ""
	 */
	scoring = '';

	/**
	 * Configuration of fields like a QuestionType.multiTextField.
	 *
	 * @type {Array<MultiFieldConfig>}
	 */
	fields: Array<MultiFieldConfig>;

	// -----------------------------
	//  Calculated properties
	// -----------------------------

	/**
	 * Answer model for the question.
	 * <b>It's calculated field.</b>
	 */
	answer: Answer;

	/**
	 * Question has answer.
	 */
	hasAnswer: boolean;

	/**
	 * Is true if question is mandatory.
	 * Used for fast access from question description component.
	 * <b>It's calculated field.</b>
	 */
	mandatory: boolean;

	// -----------------------------
	//  Static functions
	// -----------------------------

	/**
	 * Check question for answer.
	 *
	 * @static
	 * @param {Question} question Instance of Question model.
	 */
	static checkQuestionForAnswer(question: Question): void {
		if (question.answer) {
			let r: boolean;

			switch (question.type) {
				case QuestionType[QuestionType.multipleChoice]:
				case QuestionType[QuestionType.dropDown]:
				case QuestionType[QuestionType.singleChoice]:
					r = false;
					question.answer.selection.forEach((selected: boolean, oid: string) => r = r || selected);
					question.hasAnswer = r;
					break;
				case QuestionType[QuestionType.textArea]:
				case QuestionType[QuestionType.textField]:
					question.hasAnswer = question.answer.text && question.answer.text.length > 0;
					break;
				case QuestionType[QuestionType.multiTextField]:
					r = true;
					if (question.fields) {
						if (question.answer) {
							question.fields.forEach(f => {
								if (question.answer.multiFieldTexts && question.answer.multiFieldTexts[f.id]) {
									r = r && (question.answer.multiFieldTexts[f.id] as string).length > 0;
								} else {
									r = false;
								}
							});
						} else {
							r = false;
						}
					} else {
						r = false;
					}
					question.hasAnswer = r;
					break;
				default:
					break;
			}
		} else {
			question.hasAnswer = false;
		}
	}
}

/**
 * Configuration model for question type like a QuestionType.multiTextField.
 *
 * @export
 * @class MultiFieldConfig
 */
export interface MultiFieldConfig {
	/**
	 * Field ID.
	 *
	 * @type {string}
	 */
	id: string;
}
