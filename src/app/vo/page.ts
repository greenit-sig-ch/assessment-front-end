import {PageContent} from './page-content';

/**
 * Assessment page model.
 *
 * @export
 * @class Page
 */
export class Page {
	/**
	 * Version number.
	 */
	public version: number;

	/**
	 * The page's (human readable) identification string.
	 * It is the responsibility of the person defining an assessment
	 * to make sure that this id is unique within the assessment.
	 */
	public id: string;

	/**
	 * The title that is displayed for this page in the navigation tree.
	 * <b>Is localizable</b>.
	 */
	public menu: string;

	/**
	 * The page title.
	 * <b>Is localizable</b>.
	 */
	public title: string;

	/**
	 * Text that is displayed between the page title and the first question.
	 * <b>Is localizable</b>.
	 */
	public desc: string;

	/**
	 * Page type.
	 * Contains string value one of:
	 * <ul>
	 *    <li><b>question</b></li>
	 *    <li><b>evaluation</b></li>
	 *    <li><b>export</b></li>
	 * </ul>
	 * Use enumeration <b>PageType</b>
	 * @see PageType
	 */
	public type: string;

	/**
	 * The id of the page that is displayed when the user clicks on the next page button.
	 * If this property is empty, then the next enabled page in the page hierarchy is
	 * displayed (using pre-order traversal).
	 *
	 * @default ""
	 * @type {string}
	 */
	public nextPageId = '';

	/**
	 * The id of the string resource used for the label of the next page button.
	 * Default value: "NextPage".
	 */
	public nextPageLabel: string;

	/**
	 * Used during evaluation.
	 *   0 --> Do not print this page in the summary report.
	 *   1 --> Print the results of this page in the summary report.
	 *   2 --> Print the results and the questions of this page in the summary report.
	 */
	public printSummary: number;

	/**
	 * Used during evaluation.
	 *   0 --> Do not print this page in the detailed report.
	 *   1 --> Print the results of this page in the detailed report.
	 */
	public printDetailed: number;

	/**
	 * An array of questions referenced by the rid property.
	 */
	public contents: Array<PageContent>;

	// -----------------------------
	//  Calculated properties
	// -----------------------------

	/**
	 * Link to root page (page with level 0).
	 * If undefined then it is not root.
	 *
	 * @type {Page}
	 */
	public rootPage: Page;

	/**
	 * Page nesting level.
	 * Can be used for styling CSS.
	 */
	public pageLevel: number;

	/**
	 * Actual page number like a X.Y.Z according to selection.
	 */
	public showPageNumber: string;

	/**
	 * Indicate that all questions is answered on this page.
	 *
	 * @type {boolean}
	 */
	public isFullyAnswered = false;

	/**
	 * If showQuestionNumber is defined for the current page, then take the value of currentPage.showQuestionNumber.
	 * If currentPage.showQuestionNumber is not defined, then take the value of configuration.showQuestionNumber.
	 *
	 * @type {boolean}
	 */
	public showQuestionNumber = true;
}

/**
 * Page types enumeration.
 *
 * @export
 * @enum {number}
 */
export enum PageType {
	/**
	 * Question page type.
	 */
	question,
	/**
	 * Evaluation page type.
	 */
	evaluation,
	/**
	 * Export page type.
	 */
	export
}
