import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ConfigService } from './config.service';
import { LoggerService, MODE } from './logger.service';
import { RestApiService } from './rest-api.service';

import { LocalizationPipe } from '../pipe/localization.pipe';

import { Answer } from '../vo/answer';
import { Node } from '../vo/node';
import { Option, OptionType } from '../vo/option';
import { Page, PageType } from '../vo/page';
import { Question, QuestionType } from '../vo/question';
import { Questionnaire } from '../vo/questionnaire';
import { SaveAnswer } from '../vo/save-answer';
import { IInitializationData } from './http.service';
import { PageContent } from '../vo/page-content';
import { ToastrService } from 'ngx-toastr';

// -----------------------------
//  Constants
// -----------------------------

const FUNC_ON_INIT = 'onInit';
const FUNC_OBJECT = 'questionnaireFunctions';
const FUNC_ON_PAGE_LOAD = 'handlerOnPageLoad';

/**
 * Application data service.
 */
@Injectable({
	providedIn: 'root'
})
export class DataService {

	// -----------------------------
	//  Private properties
	// -----------------------------

	private readonly _structuredPages: Array<Page> = [];
	private readonly _nodeHashmap: Map<string, Node> = new Map<string, Node>();
	private readonly _pageHashmap: Map<string, Page> = new Map<string, Page>();
	private readonly _questionHashmap: Map<string, Question> = new Map<string, Question>();
	private readonly _resourceItemHashmap: Map<string, string> = new Map<string, string>();
	private readonly _lang: LocalizationPipe;

	private _currentPage: Page = new Page();
	private _previousPage: Page;
	private _currentPageIndex = 0;
	private _questionnaire: Questionnaire = new Questionnaire();
	private _questionnaireHash: string;
	private _uuid: string;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Set questionnaire for the application.
	 *
	 * @param {[string, Questionnaire]} data Instance of Questionnaire model and file hash.
	 * @returns {void}
	 */
	setQuestionnaire(data: IInitializationData): void {
		this.logger.log(MODE.log, `Set questionnaire data: ${data.questionnaire.id}, ver:${data.questionnaire.version}, hash:${data.hash}`);

		this._questionnaire = data.questionnaire;
		this._questionnaireHash = data.hash;
		this._uuid = this.createUUID();

		this.parseQuestionnaire();
		this.resetQuestionnaire();
		this.restoreQuestionnaireFromLocalStorage(data[0]);

		// check version REST API
		this.restApiService.info(); // TODO: add checking REST API ver

		// check for answers
		this._structuredPages.forEach(p => this.checkForAllAnswers(p));

		// set first page as current
		this.setCurrentPage(this._structuredPages[0]);
	}

	/**
	 * Return application script.
	 *
	 * @returns {Questionnaire} Instance of Questionnaire model.
	 */
	getQuestionnaire(): Questionnaire {
		return this._questionnaire;
	}

	// -----

	/**
	 * Return map of all question items by RID key.
	 *
	 * @returns {Map<string, Question>}
	 */
	getQuestionHashmap(): Map<string, Question> {
		return this._questionHashmap;
	}

	// -----

	/**
	 * Return structured page array.
	 *
	 * @returns {Array<Page>}
	 */
	getStructuredPages(): Array<Page> {
		return this._structuredPages;
	}

	// -----

	/**
	 * Set current page and calculate current page index.
	 *
	 * @param page The page where the transition is planned.
	 * @returns {void}
	 */
	setCurrentPage(page: Page): void {
		// check this page for mandatory questions
		if (this.checkForMandatoryPageAndShowAlert(page)) {

			// before switch to new page execute function 'handlerOnPageLoad'
			if (window.hasOwnProperty(FUNC_OBJECT)
				&& window[FUNC_OBJECT][FUNC_ON_PAGE_LOAD]
				&& typeof window[FUNC_OBJECT][FUNC_ON_PAGE_LOAD] === 'function'
			) {
				// handler exists and we should call it
				try {
					this.logger.log(MODE.log, 'Call external function:', FUNC_ON_PAGE_LOAD);
					window[FUNC_OBJECT][FUNC_ON_PAGE_LOAD](
						this.configService.getConfiguration(),
						this._questionnaire,
						this._currentPage,
						page);
				} catch (e) {
					this.logger.log(MODE.error, e);
				}
			}

			// we can transit to the new page
			this._currentPage = page;
			this._currentPageIndex = this._structuredPages.indexOf(page) + 1;
			this.logger.log(MODE.log, 'setCurrentPage =>', page.id, page.contents);

			// call tracking function of REST API service
			this.restApiService.track(this, RestApiService.EVENT_TYPE_PAGE_OPENED, page);
		}

		this.checkForAllAnswers();
	}

	// -----

	/**
	 * Returns the given question's score. The function checks if there is an
	 * external scoring function. If so, that function is called in order to
	 * calculate the score. If there is no such function, then the score according
	 * to the answer selected by the user is returned.
	 *
	 * @param {Question} question The question for which to fetch the score.
	 * @returns {number} The question's score.
	 */
	getScoreForQuestion(question: Question): number {
		if (!question.answer) {
			return 0;
		}

		let result = 0;
		if (question.scoring) {
			if (window.hasOwnProperty(FUNC_OBJECT)
				&& window[FUNC_OBJECT][question.scoring]
				&& typeof window[FUNC_OBJECT][question.scoring] === 'function') {

				// handler exists and we should call it
				try {
					this.logger.log(MODE.log, 'Call external function:', question.scoring);
					result = window[FUNC_OBJECT][question.scoring](question);
				} catch (e) {
					this.logger.log(MODE.error, e);
				}
			}
		} else {
			let isRelevant = true;
			question.options.forEach(o => {
				if (question.answer.selection.get(o.id)) {
					if (o.type === OptionType[OptionType.nr]) {
						isRelevant = false;
					}
					if (isRelevant) {
						result = o.score;
					}
				}
			});
		}

		return result;
	}

	/**
	 * Returns the given question's maximum score. If the user's answer to this
	 * question is that it is not relevant, then 0 is returned; otherwise, the
	 * question's defined maximum score is returned.
	 *
	 * @param {Question} question The question for which to fetch the maximum score.
	 * @returns {number} The question's maximum score.
	 */
	getMaxScoreForQuestion(question: Question): number {

		if (!question.answer) {
			return 0;
		}

		let isRelevant = true;
		question.options.forEach(o => {
			if (question.answer.selection.get(o.id) && o.type === OptionType[OptionType.nr]) {
				isRelevant = false;
			}
		});

		return isRelevant ? question.maxScore : 0;
	}

	// -----

	/**
	 * Returns the page instance for a given page id.
	 *
	 * @param {string} pageId The id for which to fetch the page instance.
	 * @returns {Page} The page instance associated with the given id.
	 */
	getPageForId(pageId: string): Page | null {
		if (this._pageHashmap.has(pageId)) {
			return this._pageHashmap.get(pageId);
		}

		this.logger.log(MODE.error, 'getPageForId() called with non-existing key.');

		return null;
	}

	/**
	 * Return current active page.
	 *
	 * @returns {Page} Instance of current active page.
	 */
	getCurrentPage(): Page {
		return this._currentPage;
	}

	// -----

	/**
	 * Set previous page.
	 *
	 * @param {Page} page Instance of current active page.
	 */
	setPreviousPage(page: Page): void {
		this._previousPage = page;
	}
	/**
	 * Return link to previous page.
	 *
	 * @return {Page} page
	 */
	getPreviousPage(): Page {
		return this._previousPage;
	}

	// -----

	/**
	 * Return index of current selected page.
	 *
	 * @returns {number} Numeric value with actual page index.
	 */
	getCurrentPageIndex(): number {
		return this._currentPageIndex;
	}

	// -----

	/**
	 * Return hashmap with localization resources.
	 *
	 * @returns {Map<string, string>}
	 */
	getResourceItemHashMap(): Map<string, string> {
		return this._resourceItemHashmap;
	}

	// -----

	/**
	 * Return hash for current questionnaire.
	 *
	 * @returns {string}
	 */
	getQuestionnaireHash(): string {
		return this._questionnaireHash;
	}

	// -----

	/**
	 * Return UUID for current questionnaire.
	 *
	 * @returns {string}
	 */
	getUUID(): string {
		return this._uuid;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of DataService.
	 *
	 * @param router
	 * @param restApiService
	 * @param logger
	 * @param configService
	 * @param toastrService
	 */
	constructor(
		private readonly router: Router,
		private readonly restApiService: RestApiService,
		private readonly logger: LoggerService,
		private readonly configService: ConfigService,
		private readonly toastrService: ToastrService,
	) {
		this._lang = new LocalizationPipe(this.configService);
	}

	/**
	 * Update answers inside questionnaire by array of answers.
	 *
	 * @param {Array<Answer>} answers Array of answers.
	 * @returns {void}
	 */
	setAnswersToQuestionnaire(answers: Array<Answer>): void {
		answers.forEach(answer => {
			const question: Question = this._questionHashmap.get(answer.qid);
			question.answer = answer;
		});
		// Update the hasAnswer property for each question and the isFullyAnswered
		// property of each page.
		this._structuredPages.forEach(page => {
			this.checkForAllAnswers(page);
		});
	}

	/**
	 * Check mandatory questions.
	 *
	 * @param {Page} page Page instance.
	 * @return {boolean}
	 */
	isMandatoryPage(page: Page): boolean {
		if (!Array.isArray(page.contents) || !page.contents.length) {
			return false;
		}

		let result = true;
		page.contents.forEach(pageContent => {
			// get question from hashmap
			const question: Question = this._questionHashmap.get(pageContent.rid);

			// check question if it is mandatory
			if (question && question.mandatory) {
				result = result && question.hasAnswer;
			}
		});

		return !result;
	}

	/**
	 * Save all answers to local storage by key <b>QUESTIONNAIRE.ID-QUESTIONNAIRE.VERSION<b/>
	 *
	 * @returns {void}
	 */
	saveCurrentAnswers(): void {
		const saveAnswer: SaveAnswer = new SaveAnswer(this._questionnaire, this._questionnaireHash, this._uuid);
		const json = JSON.stringify(saveAnswer);
		localStorage.setItem(this.getStorageKey(), json);
	}

	/**
	 * Reset questionnaire properties.
	 *
	 * @returns {void}
	 */
	resetQuestionnaire(): void {
		this._questionnaire.questions.forEach(question => {
			question.answer = new Answer();
			question.answer.qid = question.id;
			question.answer.comment = '';
			question.answer.text = '';
			question.answer.selection = new Map<string, boolean>();
			question.options.forEach((option: Option) => question.answer.selection.set(option.id, false));
		});

		this._structuredPages.forEach(p => p.isFullyAnswered = false);
	}

	/**
	 * Check for mandatory page and show alert if page did not filled completely.
	 *
	 * @param {Page} page Page for checking.
	 * @returns {boolean} True if page was filled completely or false if not all answers was done.
	 */
	checkForMandatoryPageAndShowAlert(page: Page): boolean {
		const showAlert = (this._currentPage.type === PageType[PageType.question]
			&& this._structuredPages.indexOf(page) > this._structuredPages.indexOf(this._currentPage)
			&& this.isMandatoryPage(this._currentPage));

		// show warning for mandatory page
		if (showAlert) {
			this.toastrService.warning(
				this._lang.transform('MandatoryWarningMessage'),
				this._lang.transform('MandatoryWarning')
			);
			// this.messageService.add({
			// 	severity: 'warn',
			// 	summary: this._lang.transform('MandatoryWarning'),
			// 	detail: this._lang.transform('MandatoryWarningMessage'),
			// });
		}

		return !showAlert;
	}

	/**
	 * Parse answered option.
	 * This function will check <b>onAnswerGiven</b> question property if it exists.
	 * If the function exists it will been called.
	 *
	 * @param {Page} page
	 * @param {Question} question
	 * @param {Option} option
	 * @returns {void}
	 */
	parseAnsweredOption(page: Page, question: Question, option: Option): void {
		const answerFunc: string = question.onAnswerGiven;
		if (answerFunc) {
			if (window.hasOwnProperty(FUNC_OBJECT)
					&& window[FUNC_OBJECT][answerFunc]
					&& typeof window[FUNC_OBJECT][answerFunc] === 'function') {
				try {
					// call function
					window[FUNC_OBJECT][answerFunc](
						this.configService.getConfiguration(),
						this._questionnaire,
						page,
						question,
						option
					);

					this.logger.log(MODE.log, `Called external function <${question.onAnswerGiven}>`
						+ `feedbackId=${question.feedbackId}, `
						+ `nextButtonId=${page.nextPageId}`
					);
				} catch (e) {
					this.logger.log(MODE.error, `Function <${answerFunc}> has an error!`, e);
				}
			} else {
				this.logger.log(MODE.error, `Function <${answerFunc}> does not have implementation`);
			}
		}
	}

	/**
	 * Check current page for all answered questions.
	 * This function will change <b>isFullyAnswered</b> property in Page model
	 * and also hasAnswer <b>property</b> in Question model.
	 *
	 * @param {Page} page
	 * @returns {void}
	 */
	public checkForAllAnswers(page?: Page): void {
		const p: Page = page ? page : this._currentPage;

		let question: Question;
		if (p.type === PageType[PageType.question]) {
			let r = true;

			if (p.rootPage) {
				// parse questions
				p.contents.forEach(pc => {
					question = this._questionHashmap.get(pc.rid);
					if (question) {
						Question.checkQuestionForAnswer(question);
						r = r && question.hasAnswer;
					}
				});
				p.isFullyAnswered = r;

				// check that all questions is answered
				// and setup property isFullyAnswered for the root page
				r = true;
				this._structuredPages
					.filter(sp => sp && sp.rootPage && sp.rootPage.id === p.rootPage.id)
					.forEach((ssp, idx) => r = r && ssp.isFullyAnswered);
				p.rootPage.isFullyAnswered = r;
			} else {
				// check that all questions is answered
				this._structuredPages
					.filter(sp => sp && sp.rootPage && sp.rootPage.id === p.id)
					.forEach((ssp, idx) => r = r && ssp.isFullyAnswered);
				p.isFullyAnswered = r;
			}
		}
	}

	/**
	 * Call external handler onInit.
	 * There are instances when new answer objects are created:
	 * - when the user uses the assessment app for the first time,
	 * - when previous answers must be deleted because the questionnaire version number has changed,
	 * - when the user resets his answers.
	 * After the "empty" answers have been created, a custom function in handler.js must be called.
	 * The name of the function is onInit and the parameters are the Configuration and the Questionnaire objects.
	 *
	 * @returns {void}
	 */
	callHandlerOnInit(): void {
		if (window.hasOwnProperty(FUNC_OBJECT)
				&& window[FUNC_OBJECT][FUNC_ON_INIT]
				&& typeof window[FUNC_OBJECT][FUNC_ON_INIT] === 'function') {
			try {
				this.logger.log(MODE.log, 'Call external function:', FUNC_ON_INIT);
				window[FUNC_OBJECT][FUNC_ON_INIT](
					this.configService.getConfiguration(),
					this._questionnaire);
			} catch (e) {
				this.logger.log(MODE.error, e);
			}
		}
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Generates a GUID string.
	 * Returns the generated GUID like a <b>af8a8416-6e18-a307-bd9c-f2c947bbb3aa</b>
	 *
	 * @returns {string}
	 */
	private createUUID(): string {
		const s: Array<string> = [];
		const hexDigits = '0123456789abcdef';
		for (let i = 0; i < 36; i++) {
			s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
		}
		s[14] = '4';
		s[19] = hexDigits.substr((Number(s[19]) & 0x3) | 0x8, 1);
		s[8] = s[13] = s[18] = s[23] = '-';

		return s.join('');
	}

	/**
	 * Parse script after loading.
	 * Prepare all variables for correct application work.
	 *
	 * @returns {void}
	 */
	private parseQuestionnaire(): void {
		if (this._questionnaire.structure !== undefined
				&& this._questionnaire.pages !== undefined
				&& this._questionnaire.pages.length > 0
				&& this._questionnaire.structure.length > 0) {

			// clear
			this._structuredPages.splice(0, this.getStructuredPages().length);

			// make pages hashmaps
			this._pageHashmap.clear();
			this._questionnaire.pages.forEach((page: Page) => {
				this._pageHashmap.set(page.id, page);
			});

			// parse structure and pages to array
			this._nodeHashmap.clear();
			this._questionnaire.structure.forEach((structure, idx) => {
				this.checkNode(structure, '', idx, 0, this._pageHashmap.get(structure.rid));
			});

			// make questions hashmaps
			this._questionHashmap.clear();
			this._questionnaire.questions.forEach((question: Question) => {
				if (question.options === undefined) {
					question.options = [];
				}
				if (question.actionIds === undefined) {
					question.actionIds = [];
				}
				question.mandatory = this.checkMandatoryProperty(question);
				this._questionHashmap.set(question.id, question);
			});
		} else {
			this.logger.log(MODE.error, 'Parsing fail! Check the SCRIPT file!');
		}
	}

	/**
	 * Parse structure and it children's array.
	 *
	 * @param structure Node item which will be parsed.
	 * @param prefix String prefix which will be added to start of number page string.
	 * @param index Parent child index.
	 * @param level Ppage level. For root page it equals 0.
	 * @param rootPage Link to root page.
	 * @returns {void}
	 */
	private checkNode(structure: Node, prefix: string, index: number, level: number, rootPage: Page): void {
		this._nodeHashmap.set(structure.rid, structure as Node);

		// add new page to array
		const key: string = structure.rid;
		if (this._pageHashmap.has(key)) {
			// get page from hash and update a calculated fields
			const page: Page = this._pageHashmap.get(key);
			page.rootPage = (level === 0 ? undefined : rootPage);
			page.pageLevel = level;
			page.showPageNumber = prefix + (index + 1).toString();

			this.logger.log(MODE.log, 'Parsed page ['
				+ this._structuredPages.length
				+ ']=> ' + page.id
				+ ', page number:' + page.showPageNumber
				+ ', level:' + level
				+ ', parent:' + (page.rootPage ? page.rootPage.id : page.rootPage));
			this._structuredPages.push(page);

			// node has children's?
			if (structure.children !== undefined && structure.children.length > 0) {
				const newLevel = level + 1;
				structure.children.forEach((node, idx) => {
					this.checkNode(node, `${page.showPageNumber}.`, idx, newLevel, rootPage);
				});
			}
		} else {
			this.logger.log(MODE.warn, `RID:<${key}> not found! It will be skipped:`, structure);
		}
	}

	/**
	 * Check question for mandatory answers.
	 *
	 * @param {Question} question Question which we will check.
	 * @returns {boolean}
	 */
	private checkMandatoryProperty(question: Question): boolean {
		let result = false;
		this._structuredPages.forEach(page => {
			if (page.contents && page.contents.length) {
				page.contents.forEach(pageContent => {
					if (pageContent.rid === question.id) {
						result = pageContent.mandatory;
					}
				});
			}
		});

		return result;
	}

	/**
	 * Return key for local storage based on questionnaire ID and version.
	 *
	 * @returns {string}
	 */
	private getStorageKey(): string {
		return `${this._questionnaire.id}-${this._questionnaire.version}`;
	}

	/**
	 * Read questionnaire from local storage.
	 *
	 * @param {string} md5hash Hash of questionnaire from loaded script.
	 * @param {boolean} ignoreHash Ignore the hash differences.
	 * @returns {boolean} Return <b>true</b> if key exist or <b>false</b> otherwise.
	 */
	private restoreQuestionnaireFromLocalStorage(md5hash: string, ignoreHash: boolean = true): boolean {
		// check if answers exist in local storage
		const json = localStorage.getItem(this.getStorageKey());
		if (json) {
			this.logger.log(MODE.log, 'Exists answers by key:', this.getStorageKey());
			const saveAnswer: SaveAnswer = JSON.parse(json);

			// check loaded questionnaire hash and hash from local storage
			const hashIsEqual = (md5hash === saveAnswer.hash);
			if (hashIsEqual || ignoreHash) {
				if (hashIsEqual) {
					this.logger.log(MODE.log, `Answers hash is OK: ${saveAnswer.hash}, now will used UUID: ${saveAnswer.uuid}`);
				} else {
					// check answer structure if hash is different
					this.logger.log(MODE.warn, 'Hash is different! Application will try to fill answers...');
					if (!Array.isArray(saveAnswer.answers)) {
						this.logger.log(MODE.warn, 'Loaded answers has incompatible properties and will skipped!');

						return false;
					}
				}

				// restore answers
				saveAnswer.answers.forEach(o => {
					const answer: Answer = Answer.fromSimpleObject(o);
					if (this._questionHashmap.has(answer.qid)) {
						const question: Question = this._questionHashmap.get(answer.qid);
						question.answer = answer;

						// parse answer for singleChoice
						if (question.type === QuestionType[QuestionType.singleChoice]) {
							question.options.forEach(qo => {
								if (question.answer.selection.get(qo.id)) {
									// find page for question
									let fp: Page = null;
									this._structuredPages.some(p => p.contents.some((pc: PageContent) => {
										if (pc.rid === question.id) {
											fp = p;

											return true;
										}
										
										return false;
									}));

									this.parseAnsweredOption(fp, question, qo);
								}
							});
						}
					} else {
						this.logger.log(MODE.warn, `Answer "${answer.qid}" doesn't exists. It will skipped!`, o);
					}
				});

				// restore UUID
				this._uuid = saveAnswer.uuid;
			}

			return true;
		}

		this.logger.log(MODE.log, `Can't found existing answer!`);
		this.callHandlerOnInit();

		return false;
	}
}
