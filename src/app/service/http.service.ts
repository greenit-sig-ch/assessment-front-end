import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { forkJoin, Observable, Subject } from 'rxjs';

import * as FileSaver from 'file-saver';
import { Md5 } from 'ts-md5/dist/md5';

import { LoggerService, MODE } from './logger.service';

import { Configuration } from '../vo/configuration';
import { Questionnaire } from '../vo/questionnaire';

import * as jsyaml from 'js-yaml';

/**
 * Initialization data model.
 */
export interface IInitializationData {
	hash: string;
	configuration: Configuration;
	questionnaire: Questionnaire;
}

// -----------------------------
//  Constants
// -----------------------------

export const QUESTIONNAIRE = 'questionnaire.json';
export const CONFIG_FILE = 'content/config.yaml';
export const COMMON_LANG = 'assets/assessment-front-end-';
export const YAML = '.yaml';

/**
 * Application HTTP service.
 *
 * @export
 * @class HttpService
 */
@Injectable({
	providedIn: 'root'
})
export class HttpService {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of HttpService.
	 *
	 * @param {LoggerService} logger
	 * @param {HttpClient} http
	 */
	constructor(
		private readonly logger: LoggerService,
		private readonly http: HttpClient
	) {}

	/**
	 * Download file to local computer.
	 *
	 * @param {string} data String data to save.
	 * @param {string} name File name.
	 * @return {void}
	 */
	downloadFile(data: string, name: string): void {
		const file = new Blob([data], {type: 'text/plain;charset=utf-8'});
		FileSaver.saveAs(file, name);
	}

	/**
	 * Load configuration and questionnaire files.
	 *
	 * @returns {Observable<IInitializationData>}
	 */
	loadConfigurationAndQuestionnaire(): Observable<IInitializationData> {
		this.logger.log(MODE.log, 'Loading configuration and script files...');

		const result = new Subject<IInitializationData>();
		this.http.get(CONFIG_FILE, {responseType: 'text'})
			.subscribe(responseConfig => {
				const configuration: Configuration = jsyaml.load(responseConfig) as Configuration;
				this.http.get(configuration.scriptFile, {responseType: 'text'})
					.subscribe(questionnaireResponse => {
						this.logger.setCurrentMode(configuration.logLevel);

						const questionnaire: Questionnaire = jsyaml.load(questionnaireResponse) as Questionnaire;
						const hash = Md5.hashStr(questionnaireResponse) as string;

						result.next({hash, configuration, questionnaire});
					}, errorQuestionnaire => {
						result.error(errorQuestionnaire);
					});
			}, errorConfig => {
				result.error(errorConfig);
			});

		return result;
	}

	/**
	 * Load languages files by filename list.
	 *
	 * @param {Array<string>} langFiles List of files to load.
	 */
	loadLanguageFiles(langFiles: Array<string>): Observable<Map<string, string>> {
		this.logger.log(MODE.log, 'Loading files:', langFiles);

		const result = new Subject<Map<string, string>>();
		const map = new Map<string, string>();
		const observables = langFiles.map(observable => {
			return this.http.get(observable, {responseType: 'text'});
		});
		forkJoin(observables)
			.subscribe(responses => {
				if (responses) {
					responses.forEach(res => {
						const obj = jsyaml.load(res);
						Object.keys(obj)
							.forEach(key => map.set(key, obj[key]));
					});
				}
			}, error => {
				result.error(error);
			}, () => {
				result.next(map);
			});

		return result;
	}

	// /**
	//  * Load language files.
	//  *
	//  * @param {string} langFile
	//  * @param {string} commonLangFile
	//  * @returns {Observable<Map<string, string>>}
	//  */
	// loadLanguageFiles(langFile: string, commonLangFile: string): Observable<Map<string, string>> {
	// 	this.logger.log(MODE.log, 'Loading files:', langFile, commonLangFile);
	//
	// 	const result = new Subject<Map<string, string>>();
	// 	this.http.get(langFile, {responseType: 'text'})
	// 		.subscribe(responseLang => {
	// 			this.http.get(commonLangFile, {responseType: 'text'})
	// 				.subscribe(responseCommonLang => {
	// 					const obj1: any = jsyaml.load(responseLang);
	// 					const obj2: any = jsyaml.load(responseCommonLang);
	// 					const map: Map<string, string> = new Map<string, string>();
	// 					Object.keys(obj1).forEach(key => map.set(key, obj1[key]));
	// 					Object.keys(obj2).forEach(key => map.set(key, obj2[key]));
	// 					result.next(map);
	// 				}, errorCommonLang => result.error(errorCommonLang))
	// 		}, errorLang => result.error(errorLang));
	//
	// 	return result;
	// }
}
