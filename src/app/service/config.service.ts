import { Injectable } from '@angular/core';

import { Environment } from '../interfaces/environment';
import { Configuration } from '../vo/configuration';
import { LoggerService, MODE } from './logger.service';

/**
 * Config service.
 *
 * @export
 * @class ConfigService
 */
@Injectable()
export class ConfigService {

	// -----------------------------
	//  Private properties
	// -----------------------------

	private _configuration: Configuration = new Configuration();
	private readonly _environment: Environment = {
		isChrome: false,
		isFirefox: false,
		isIE: false,
		isLinux: false,
		isMac: false,
		isSafari: false,
		isWindows: false,
		locale: undefined,
		version: undefined
	};

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	get environment(): Environment {
		return this._environment;
	}

	// -----

	/**
	 * Set application configuration.
	 *
	 * @param {Configuration} configuration Instance of Configuration model.
	 */
	setConfiguration(configuration: Configuration): void {
		this.logger.log(MODE.log, 'Application configuration:', configuration);
		this._configuration = configuration;
	}
	/**
	 * Return application configuration.
	 */
	getConfiguration(): Configuration {
		return this._configuration;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of ConfigService.
	 *
	 * @param {LoggerService} logger
	 */
	constructor(
		private readonly logger: LoggerService
	) {}

	/**
	 * Init application.
	 *
	 * @return {void}
	 */
	initConfiguration(): void {
		this.logger.log(MODE.log, 'Init application on load...');
		this.detectEnvironment();
	}

	/**
	 * Parse URL parameters.
	 *
	 * @return {Map<string, string>}
	 */
	parseURLParameters(): Map<string, string> {
		const result = new Map<string, string>();

		if (window.location.search && window.location.search.length > 0) {
			const urlStr: string = window.location.search.substring(1);
			urlStr.split('&')
				.forEach(p => {
					const arr: Array<string> = p.split('=');
					if (arr.length === 2) {
						result.set(arr[0], arr[1]);
					}
				});
		}

		return result;
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Detect system environment.
	 *
	 * @returns {void}
	 */
	private detectEnvironment(): void {
		const navigator: Navigator = window.navigator;
		const platform: string = navigator.platform;
		const appVersion: string = navigator.appVersion;
		const userAgent: string = navigator.userAgent;

		this._environment.isWindows = /^win32/i.test(platform) || /win/i.test(appVersion);
		this._environment.isMac = /^mac/i.test(platform) || /mac/i.test(appVersion);
		this._environment.isLinux = /^linux/i.test(platform) || /linux/i.test(appVersion);

		// MSIE check taken from Angular core
		const ie: number = Number(document['documentMode']);
		this._environment.isChrome = /chrome/i.test(userAgent);
		this._environment.isFirefox = /firefox/i.test(userAgent);
		this._environment.isSafari = /safari/i.test(userAgent);
		this._environment.isSafari = !this._environment.isChrome && this._environment.isSafari;

		let arr: RegExpMatchArray = userAgent.match(/version\/(\d+(\.\d+)?)/i);
		let version: string = arr && arr.length ? arr[1] : undefined;
		if (!isNaN(ie)) {
			this._environment.isIE = true;
			arr = userAgent.match(/(?:msie |rv:)(\d+(\.\d+)?)/i);
			version = arr && arr.length ? arr[1] : undefined;
		}
		this._environment.version = parseFloat(version);

		// detect locale
		const browserLanguagePropertyKeys = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'];
		if (Array.isArray(navigator.languages)) {
			this._environment.locale = navigator.languages.find(l => l && String(l).length > 0);
		} else {
			const lv = browserLanguagePropertyKeys.find(lk =>	navigator[lk] && String(navigator[lk]).length > 0);
			this._environment.locale = navigator[lv];
		}

		this.logger.log(MODE.log, 'Current environment configuration:', this._environment);
	}
}
