import {TestBed, inject} from '@angular/core/testing';

import {HttpService} from './http.service';
import {DataService} from './data.service';

import {AppComponent} from '../app.component';
import {PageComponent} from '../view/pages/page.component';
import {PageTitleComponent} from '../view/pages/title/page-title.component';
import {PageNavigatorComponent} from '../view/navigator/page-navigator.component';
import {QuestionComponent} from '../view/questions/question.component';
import {QuestionDescriptionComponent} from '../view/questions/description/question-description.component';
import {TextAreaComponent} from '../view/questions/text-area/text-area.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HttpService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule
			],
			declarations: [
				AppComponent,
				PageComponent,
				PageTitleComponent,
				PageNavigatorComponent,
				QuestionComponent,
				QuestionDescriptionComponent,
				TextAreaComponent
			],
			providers: [HttpService,DataService]
		});
	});

	it('should ...', inject([HttpService], (service: HttpService) => {
		expect(service).toBeTruthy();
	}));
});
