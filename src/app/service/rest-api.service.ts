import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ConfigService } from './config.service';
import { DataService } from './data.service';
import { LoggerService, MODE } from './logger.service';

import { Option } from '../vo/option';
import { Page } from '../vo/page';
import { Question, QuestionType } from '../vo/question';
import { Tracking } from '../vo/rest-api';

/**
 * Service for working with RestAPI.
 */
@Injectable({
	providedIn: 'root'
})
export class RestApiService {

	// -----------------------------
	//  Constants
	// -----------------------------

	public static EVENT_TYPE_PAGE_OPENED = 'pageOpened';
	public static EVENT_TYPE_ANSWER_GIVEN = 'answerGiven';

	public static METHOD_INFO = 'info';
	public static METHOD_TRACK = 'track';

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of RestApiService.
	 *
	 * @param {LoggerService} logger
	 * @param {ConfigService} configService
	 * @param {Http} http
	 */
	constructor(
		private logger: LoggerService,
		private configService: ConfigService,
		private http: HttpClient
	) {}

	/**
	 * [GET] Return REST API information.
	 *
	 * @returns {void}
	 */
	info(): void {
		const url: string = this.configService.getConfiguration().restApiUrl + RestApiService.METHOD_INFO;
		this.http.get(url)
			.subscribe(res => {
				this.logger.log(MODE.log, res);
			}, error => this.errorHandler(error, 'info'));
	}

	/**
	 * [POST] Track user.
	 *
	 * @param {DataService} dataService
	 * @param {string} eventType
	 * @param {Page} page
	 * @param {Question} question
	 * @param {Option} option
	 * @returns {void}
	 */
	track(
		dataService: DataService,
		eventType: string,
		page: Page = null,
		question: Question = null,
		option: Option = null
	): void {

		if ((eventType === RestApiService.EVENT_TYPE_PAGE_OPENED
				&& !this.configService.getConfiguration().trackPageOpen)
						|| (eventType === RestApiService.EVENT_TYPE_ANSWER_GIVEN
								&& !this.configService.getConfiguration().trackQuestions
								&& !this.configService.getConfiguration().trackAnswers)) {
			return;
		}

		const tracking: Tracking = new Tracking();
		tracking.eventType = eventType;
		tracking.uuid = dataService.getUUID();
		tracking.assessmentId = dataService.getQuestionnaire().id;
		tracking.assessmentVersion = dataService.getQuestionnaire().version;
		tracking.language = this.configService.getConfiguration().scriptLanguage;

		if (this.configService.getConfiguration().trackPageOpen
				&& page
				&& RestApiService.EVENT_TYPE_PAGE_OPENED) {
			tracking.pageId = page.id;
			tracking.pageVersion = page.version;
		}

		if (question
				&& eventType === RestApiService.EVENT_TYPE_ANSWER_GIVEN
				&& (this.configService.getConfiguration().trackQuestions
					|| this.configService.getConfiguration().trackAnswers)) {
			tracking.questionId = question.id;
			tracking.questionVersion = question.version;
		}

		if (this.configService.getConfiguration().trackAnswers
				&& question && question.answer
				&& eventType === RestApiService.EVENT_TYPE_ANSWER_GIVEN) {
			tracking.optionId = option
				? option.id
				: null;
			tracking.optionText = question.answer.text && question.answer.text.length
				? question.answer.text
				: null;
			tracking.optionComment = question.answer.comment;
		}

		// Send data
		this.send(JSON.stringify(tracking), 'track');
	}

	/**
	 * [POST] Track answers given in multi-text fields.
	 *
	 * @param {DataService} dataService
	 * @param {Question} question
	 * @param {string} fieldId id
	 * @returns {void}
	 */
	public trackMultiText(dataService: DataService, question: Question, fieldId: string): void {

		// Do not track questions nor answers --> Return immediately
		if ((!this.configService.getConfiguration().trackQuestions
				&& !this.configService.getConfiguration().trackAnswers)
				|| question.type !== QuestionType[QuestionType.multiTextField]) {
			return;
		}

		const tracking: Tracking = new Tracking();
		tracking.eventType = RestApiService.EVENT_TYPE_ANSWER_GIVEN;
		tracking.uuid = dataService.getUUID();
		tracking.assessmentId = dataService.getQuestionnaire().id;
		tracking.assessmentVersion = dataService.getQuestionnaire().version;
		tracking.language = this.configService.getConfiguration().scriptLanguage;
		tracking.pageId = dataService.getCurrentPage().id;
		tracking.pageVersion = dataService.getCurrentPage().version;
		tracking.questionId = question.id;
		tracking.questionVersion = question.version;

		if (this.configService.getConfiguration().trackAnswers && question.answer) {
			tracking.optionId = fieldId;
			tracking.optionText = question.answer.multiFieldTexts[fieldId]
				&& question.answer.multiFieldTexts[fieldId].length
					? question.answer.multiFieldTexts[fieldId]
					: null;
			tracking.optionComment = question.answer.comment;
		}

		// Send data
		this.send(JSON.stringify(tracking), 'trackMultiText');
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Error handler for the file loader.
	 *
	 * @returns {void}
	 */
	private errorHandler(error: any, src: string = ''): void {
		this.logger.log(MODE.error, `RestApiService error! (function -> ${src})`, error);
	}

	/**
	 * Send tracking data to the server.
	 *
	 * @param {string} data
	 * @param {string} src
	 * @returns {void}
	 */
	private send(data: string, src: string): void {
		const url: string = this.configService.getConfiguration().restApiUrl + RestApiService.METHOD_TRACK;
		this.http.post(url, data)
			.subscribe(res => {
				this.logger.log(MODE.log, res);
			}, error => this.errorHandler(error, src));
	}
}
