import { Component, OnInit } from '@angular/core';

import { LocalizationPipe } from '../../pipe/localization.pipe';
import { DataService } from '../../service/data.service';

@Component({
	selector: 'app-print-page',
	templateUrl: './print-page.component.html',
	styleUrls: ['./print-page.component.less']
})
export class PrintPageComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	get questionnaireTitle(): string {
		return this.dataService.getQuestionnaire().title;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		private readonly dataService: DataService,
		private readonly lang: LocalizationPipe
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}

}
