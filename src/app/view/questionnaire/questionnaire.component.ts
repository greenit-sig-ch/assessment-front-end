import { animate, state, style, transition, trigger } from '@angular/animations';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';

import { LocalizationPipe } from '../../pipe/localization.pipe';
import { ConfigService } from '../../service/config.service';

import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { DataService } from '../../service/data.service';

@Component({
	selector: 'app-questionnaire',
	templateUrl: './questionnaire.component.html',
	styleUrls: ['./questionnaire.component.less'],
	animations: [
		trigger('slideInOut', [
			state('in', style({
				transform: 'translate3d(0, 0, 0)'
			})),
			state('out', style({
				transform: 'translate3d(-100%, 0, 0)'
			})),
			transition('in => out', animate('400ms ease-in-out')),
			transition('out => in', animate('400ms ease-in-out'))
		])
	]
})
export class QuestionnaireComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------

	@ViewChild('lgModal', { static: true })
	lgModal: any;

	@ViewChild('importAlert', { static: true })
	importAlert: any;

	readonly isMobile$: Observable<boolean>;

	menuState = 'out';

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	get isVisiblePageNavigator(): boolean {
		return this.configService.getConfiguration().showPageNavigator;
	}

	get bootstrapSizeForNavigator(): string {
		return 'col-lg-2 col-md-3 col-sm-3 col-xs-0 hidden-xs';
	}

	get bootstrapSizeForPage(): string {
		const showNav: boolean = this.configService.getConfiguration().showPageNavigator;

		return 'col-lg-' + (showNav ? '10' : '12') + ' '
			+ 'col-md-' + (showNav ? '9' : '12') + ' '
			+ 'col-sm-' + (showNav ? '9' : '12') + ' '
			+ 'col-xs-' + (showNav ? '12' : '12');
	}

	get logoFileName(): string {
		// Avoid 404 errors when the browser tries to get logo_undefined.png before the configuration is read.
		if (typeof this.configService.getConfiguration().scriptLanguage !== 'undefined') {
			return `content/img/logo_${this.configService.getConfiguration().scriptLanguage}.png`;
		}

		return '';
	}

	getActiveClassName(lang: string): string {
		return this.configService.getConfiguration().scriptLanguage === lang ? 'active' : '';
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of QuestionnaireComponent.
	 *
	 * @param configService
	 * @param lang
	 * @param breakpointObserver
	 * @param dataService
	 */
	constructor(
		readonly configService: ConfigService,
		private readonly lang: LocalizationPipe,
		private readonly breakpointObserver: BreakpointObserver,
		private readonly dataService: DataService
	) {
		this.isMobile$ = this.breakpointObserver.observe(`(max-width: 767.99px)`)
			.pipe(
				map(({ matches }: BreakpointState) => matches),
				publishReplay(1),
				refCount()
			);
	}

	/**
	 * Show/hide filter menu for small screens.
	 *
	 * @returns {void}
	 */
	toggleMenu(): void {
		this.menuState = this.menuState === 'out' ? 'in' : 'out';
	}

	/**
	 * On help button click handler.
	 *
	 * @param {boolean} isSmallFilter
	 * @returns {void}
	 */
	onHelpClicked(isSmallFilter: boolean): void {
		if (isSmallFilter) {
			this.menuState = 'out';
		}

		this.lgModal.show();
	}

	onReset(): void {
		this.importAlert.show();
	}

	onResetApproved(): void {
		this.dataService.resetQuestionnaire();
		this.dataService.saveCurrentAnswers();
		this.dataService.callHandlerOnInit();

		this.importAlert.hide();
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}
	
}
