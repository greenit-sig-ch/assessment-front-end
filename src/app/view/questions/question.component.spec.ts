import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DataService} from '../../service/data.service';

import {QuestionComponent} from './question.component';
import {QuestionDescriptionComponent} from './description/question-description.component';

describe('QuestionComponent', () => {
	let component: QuestionComponent;
	let fixture: ComponentFixture<QuestionComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [
				QuestionComponent,
				QuestionDescriptionComponent
			],
			providers: [DataService]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(QuestionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	// it('should create', () => {
	//  expect(component).toBeTruthy();
	// });
});
