import { Component, OnInit } from '@angular/core';
import { Option } from '../../../vo/option';
import { BasicQuestionTypeComponent } from '../basic-question-type.component';

@Component({
	selector: 'app-single-choice',
	templateUrl: './single-choice.component.html',
	styleUrls: ['./single-choice.component.less']
})
export class SingleChoiceComponent extends BasicQuestionTypeComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	// -----------------------------
	//  Public functions
	// -----------------------------

	// -----------------------------
	//  Overridden functions
	// -----------------------------

	override onSelectionChange(option: Option): void {
		this.currentQuestion.answer.selection.forEach((v, k) => {
			this.currentQuestion.answer.selection.set(k, false);
		});
		this.answer.selection.set(option.id, true);

		super.onSelectionChange(option);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}
}
