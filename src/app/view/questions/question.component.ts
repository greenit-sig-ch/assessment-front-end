import { Component, Input, OnInit } from '@angular/core';

import { LocalizationPipe } from '../../pipe/localization.pipe';

import { Question, QuestionType } from '../../vo/question';

/**
 * Question component.
 */
@Component({
	selector: 'app-question',
	templateUrl: './question.component.html',
	styleUrls: ['./question.component.less']
})
export class QuestionComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Current question for component.
	 */
	@Input()
	currentQuestion: Question;

	/**
	 * Current question index.
	 */
	@Input()
	questionIndex: number;

	// -----------------------------
	//  Public properties
	// -----------------------------

	readonly QuestionType = QuestionType;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Return text of feedback by Id if it present in question.
	 *
	 * @returns {string}
	 */
	get feedbackText(): string {
		if (this.currentQuestion.feedbackId && this.lang.checkResourceId(this.currentQuestion.feedbackId)) {
			return this.lang.transform(this.currentQuestion.feedbackId);
		}

		return undefined;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		private readonly lang: LocalizationPipe
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}
}
