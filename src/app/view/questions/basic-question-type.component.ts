import { Component, Input } from '@angular/core';

import { ConfigService } from '../../service/config.service';
import { DataService } from '../../service/data.service';
import { RestApiService } from '../../service/rest-api.service';
import { Answer } from '../../vo/answer';
import { Option } from '../../vo/option';
import { Question } from '../../vo/question';

/**
 * Basic question type component.
 *
 * @export
 * @class BasicQuestionTypeComponent
 */
@Component({
	template: ''
})
export class BasicQuestionTypeComponent {

	// -----------------------------
	//  Public properties
	// -----------------------------

	/**
	 * Current question index.
	 *
	 * @returns {number}
	 */
	@Input()
	questionIndex: number;

	/**
	 * Contain current question for component.
	 *
	 * @returns {Question} Current question model.
	 */
	@Input()
	currentQuestion: Question;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Return list of current options for the current question.
	 *
	 * @returns {Array<Option>} List of options entity.
	 */
	get currentOptions(): Array<Option> {
		return this.currentQuestion.options;
	}

	/**
	 * Return answer for question by current option.
	 *
	 * @returns {Answer}
	 */
	get answer(): Answer {
		return this.currentQuestion.answer;
	}

	/**
	 * Check selection for the option.
	 *
	 * @returns {boolean}
	 */
	isSelected(option: Option): boolean {
		return this.currentQuestion.answer.selection.get(option.id);
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of BasicQuestionTypeComponent.
	 *
	 * @param {DataService} dataService
	 * @param {RestApiService} restApiService
	 * @param {ConfigService} configService
	 */
	constructor(
		protected dataService: DataService,
		protected restApiService: RestApiService,
		protected configService: ConfigService
	) {}

	/**
	 * On change text area or text field modify the answer model and save it.
	 *
	 * @param {Event} event
	 * @param {boolean} isComment
	 * @returns {void}
	 */
	public onChangeValue(event: Event, isComment: boolean = false): void {
		this.dataService.saveCurrentAnswers();
		this.callTracking(null, isComment);
		this.dataService.checkForAllAnswers();
	}

	/**
	 * On change multi text field modify the answer model and save it.
	 *
	 * @param {Event} event
	 * @param {string} fieldId of the field that has been changed
	 * @returns {void}
	 */
	onChangeMultiValue(event: Event, fieldId: string): void {
		this.dataService.saveCurrentAnswers();
		this.restApiService.trackMultiText(this.dataService, this.currentQuestion, fieldId);
		this.dataService.checkForAllAnswers();
	}

	/**
	 * On change selection modify the answer model and save it.
	 * Abstract method.
	 *
	 * @param {Option} option
	 * @returns {void}
	 */
	onSelectionChange(option: Option): void {
		this.dataService.saveCurrentAnswers();
		this.dataService.parseAnsweredOption(this.dataService.getCurrentPage(), this.currentQuestion, option);
		this.callTracking(option, false);
		this.dataService.checkForAllAnswers();
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	/**
	 * Call track question method.
	 *
	 * @param {Option} option
	 * @param {boolean} isComment Was changed the comment field.
	 * @returns {void}
	 */
	private callTracking(option: Option, isComment: boolean): void {
		this.restApiService.track(
			this.dataService,
			RestApiService.EVENT_TYPE_ANSWER_GIVEN,
			this.dataService.getCurrentPage(),
			this.currentQuestion,
			option
		);
	}
}
