import { Component, OnInit } from '@angular/core';
import { BasicQuestionTypeComponent } from '../basic-question-type.component';

@Component({
	selector: 'app-text-area',
	templateUrl: './text-area.component.html',
	styleUrls: ['./text-area.component.less']
})
export class TextAreaComponent extends BasicQuestionTypeComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	// -----------------------------
	//  Public functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
	}
}
