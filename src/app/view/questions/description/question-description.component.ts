import { Component, Input, OnInit } from '@angular/core';

import { LocalizationPipe } from '../../../pipe/localization.pipe';

import { ConfigService } from '../../../service/config.service';
import { DataService } from '../../../service/data.service';

import { Question, QuestionType } from '../../../vo/question';

@Component({
	selector: 'app-question-description-component',
	templateUrl: './question-description.component.html',
	styleUrls: ['./question-description.component.less']
})
export class QuestionDescriptionComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Current question item for component.
	 */
	@Input()
	currentQuestion: Question;

	/**
	 * Current question index.
	 */
	@Input()
	questionIndex: number;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Current question number.
	 *
	 * @returns {string}
	 */
	get questionNumber(): string {
		return `${this.dataService.getCurrentPage().showPageNumber}.${this.questionIndex + 1}.`;
	}

	/**
	 * Flag which detect description visibility.
	 */
	descriptionVisible = false;

	/**
	 * Return property which indicate that we need to show description immediately.
	 *
	 * @returns {boolean}
	 */
	get showDescriptionsImmediately(): boolean {
		return this.configService.getConfiguration().showDescriptionsImmediately;
	}

	/**
	 * Flag which detect question number visibility.
	 *
	 * @returns {boolean}
	 */
	get showQuestionNumber(): boolean {
		return this.dataService.getCurrentPage().showQuestionNumber
			|| (this.dataService.getCurrentPage().showQuestionNumber === undefined
				&& this.configService.getConfiguration().showQuestionNumber);
	}

	/**
	 * Show/hide button "Additional information".
	 *
	 * @returns {boolean}
	 */
	get showButtonForAdditionalInfo(): boolean {
		if (this.configService.getConfiguration().showDescriptionsImmediately) {
			return false;
		}

		return this.configService.getConfiguration()
			.langPackHashMap
			.has(`${this.currentQuestion.id}-description`);
	}

	/**
	 * Check for mandatory question.
	 *
	 * @returns {boolean}
	 */
	get isMandatory(): boolean {
		return this.currentQuestion.mandatory;
	}

	// -----------------------------
	//  Public properties
	// -----------------------------

	readonly QuestionType = QuestionType;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of QuestionDescriptionComponent.
	 *
	 * @param {DataService} dataService
	 * @param {ConfigService} configService
	 * @param {LocalizationPipe} lang
	 */
	constructor(
		private readonly dataService: DataService,
		private readonly configService: ConfigService,
		private readonly lang: LocalizationPipe
	) {}

	/**
	 * On/off question description.
	 *
	 * @returns {void}
	 */
	toggleDescription(): void {
		this.descriptionVisible = !this.descriptionVisible;
	}

	/**
	 * Open new window with the Action app.
	 *
	 * @returns {void}
	 */
	onActionClickHandler(): void {
		const config = this.configService.getConfiguration();
		const lang: string = config.scriptLanguage;

		if (config && config.actionAppHost) {
			let url: string;
			url = typeof config.actionAppHost === 'string'
				// old way - when actionAppHost is a string
				? `${config.actionAppHost}?lang=${lang}&`
				// new way - actionAppHost is an object
				: `${config.actionAppHost[lang]}?`;

			// parse action IDs and add it to result url
			if (this.currentQuestion.actionIds && this.currentQuestion.actionIds.length > 0) {
				this.currentQuestion.actionIds.forEach((aid, idx) => {
					url += (`${idx === 0 ? '' : '&'}id=${aid}`);
				});
			}

			window.open(encodeURI(url));
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
	}
}
