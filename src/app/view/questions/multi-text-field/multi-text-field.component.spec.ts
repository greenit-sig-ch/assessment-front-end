import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {MultiTextFieldComponent} from './multi-text-field.component';

describe('MultiTextFieldComponent', () => {
	let component: MultiTextFieldComponent;
	let fixture: ComponentFixture<MultiTextFieldComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [MultiTextFieldComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MultiTextFieldComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
