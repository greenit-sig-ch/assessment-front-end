import { Component, Input, OnInit } from '@angular/core';
import { MultiFieldConfig } from '../../../vo/question';
import { BasicQuestionTypeComponent } from '../basic-question-type.component';

@Component({
	selector: 'app-multi-text-field',
	templateUrl: './multi-text-field.component.html',
	styleUrls: ['./multi-text-field.component.less']
})
export class MultiTextFieldComponent extends BasicQuestionTypeComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	@Input()
	public get textFields(): Array<MultiFieldConfig> {
		return this.currentQuestion.fields;
	}

	@Input()
	public getFieldClassName(field: MultiFieldConfig): string {
		return `${this.currentQuestion.id}-${field.id}`;
	}

	// -----------------------------
	//  Overridden functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}

}
