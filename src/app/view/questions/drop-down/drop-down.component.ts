import { Component, OnInit } from '@angular/core';
import { Option } from '../../../vo/option';
import { BasicQuestionTypeComponent } from '../basic-question-type.component';

/**
 * Question renderer like a dropdown.
 */
@Component({
	selector: 'app-drop-down',
	templateUrl: './drop-down.component.html',
	styleUrls: ['./drop-down.component.less']
})
export class DropDownComponent extends BasicQuestionTypeComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Return option value.
	 *
	 * @returns {string}
	 */
	get optionValue(): string {
		let result: string;
		this.currentQuestion.answer.selection.forEach((v, k) => {
			if (v) {
				result = k;
			}
		});

		return result;
	}

	// -----------------------------
	//  Overridden functions
	// -----------------------------

	override onSelectionChange(event: any): void {
		if (event instanceof Event && event.target) {
			const target: any = event.target;
			const oid: string = target.value;

			this.currentQuestion.answer.selection.forEach((v, k) => this.currentQuestion.answer.selection.set(k, false));
			this.answer.selection.set(oid, true);

			const option: Option = this.currentQuestion.options.find(o => o.id === oid);
			super.onSelectionChange(option);
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}

}
