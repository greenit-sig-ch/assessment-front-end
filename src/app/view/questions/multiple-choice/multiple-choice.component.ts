import { Component, OnInit } from '@angular/core';
import { Option } from '../../../vo/option';
import { BasicQuestionTypeComponent } from '../basic-question-type.component';

@Component({
	selector: 'app-multiple-choice',
	templateUrl: './multiple-choice.component.html',
	styleUrls: ['./multiple-choice.component.less']
})
export class MultipleChoiceComponent extends BasicQuestionTypeComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	// -----------------------------
	//  Public functions
	// -----------------------------

	// -----------------------------
	//  Overridden functions
	// -----------------------------

	override onSelectionChange(option: Option): void {
		const selected: boolean = this.answer.selection.get(option.id);
		this.answer.selection.set(option.id, !selected);

		super.onSelectionChange(option);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}
}
