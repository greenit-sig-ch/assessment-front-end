import { Component, OnInit } from '@angular/core';
import { BasicQuestionTypeComponent } from '../basic-question-type.component';

@Component({
	selector: 'app-text-field',
	templateUrl: './text-field.component.html',
	styleUrls: ['./text-field.component.less']
})
export class TextFieldComponent extends BasicQuestionTypeComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	// -----------------------------
	//  Public functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
	}
}
