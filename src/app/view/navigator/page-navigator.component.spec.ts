import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {DataService} from '../../service/data.service';
import {PageNavigatorComponent} from './page-navigator.component';

describe('PageNavigatorComponent', () => {
	let component: PageNavigatorComponent;
	let fixture: ComponentFixture<PageNavigatorComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PageNavigatorComponent],
			providers: [DataService]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PageNavigatorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
