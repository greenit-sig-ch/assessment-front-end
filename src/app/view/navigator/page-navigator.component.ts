import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { DataService } from '../../service/data.service';
import { Configuration } from '../../vo/configuration';
import { Page, PageType } from '../../vo/page';

/**
 * Page navigation component.
 * Shows current structure of pages.
 */
@Component({
	selector: 'app-page-navigator',
	templateUrl: './page-navigator.component.html',
	styleUrls: ['./page-navigator.component.less']
})
export class PageNavigatorComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------

	@Input() configuration: Configuration;

	// -----------------------------
	//  Output properties
	// -----------------------------

	@Output()
	readonly helpClicked = new EventEmitter<void>();

	@Output()
	readonly reset = new EventEmitter<void>();

	@Output()
	readonly toggleMenu = new EventEmitter<void>();

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Return structured page array.
	 *
	 * @returns {Array<Page>}
	 */
	get pages(): Array<Page> {
		return this.dataService.getStructuredPages();
	}

	/**
	 * Detect print menu item.
	 *
	 * @returns {boolean}
	 */
	get isVisiblePrintMenu(): boolean {
		return this.dataService.getCurrentPage().type === PageType[PageType.evaluation];
	}

	// -----------------------------
	//  Public properties
	// -----------------------------

	// @ViewChild(ResetModalAlertComponent, { static: true })
	// importAlert: ResetModalAlertComponent;

	printMode: string;
	printInfo: boolean;
	printChecked: boolean;
	printComments: boolean;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Constructor.
	 */
	constructor(
		private readonly dataService: DataService
	) {}

	/**
	 * Detect visibility of menu item.
	 *
	 * Show all root nodes and all nodes with common root node.
	 * @param {Page} page Page instance.
	 * @returns {boolean}
	 */
	isVisibleMenu(page: Page): boolean {
		return page.rootPage === undefined
			|| page === this.dataService.getCurrentPage()
			|| page.rootPage === this.dataService.getCurrentPage()
			|| page.rootPage === this.dataService.getCurrentPage().rootPage;
	}

	/**
	 * Show model dialog with reset alert.
	 *
	 * @returns {void}
	 */
	showResetAlert(): void {
		// this.importAlert.isModalShown = true;
		this.reset.emit();
	}

	/**
	 *
	 * @param {Page} page
	 * @returns {string}
	 */
	getMenuGlyphiconClass(page: Page): string {
		switch (page.type) {
			case PageType[PageType.question]:
				return page && page.isFullyAnswered ? 'pageIsAnswered' : 'pageNotAnswered';
			case PageType[PageType.evaluation]:
				return 'evaluationPage';
			case PageType[PageType.export]:
				return 'exportPage';
			default:
				return 'nonQuestionPage';
		}
	}

	/**
	 * Return class name <b>pnc-current-page</b> for current page.
	 *
	 * @param {Page} page Page instance.
	 * @returns {string}
	 */
	getClassForCurrentPage(page: Page): string {
		return this.dataService.getCurrentPage() === page ? 'pnc-current-page' : '';
	}

	/**
	 * Return additional class for page level like a <b>'level-X'</b>
	 * where X is value from 0 to N according to nested level.
	 *
	 * @param {Page} page Page instance.
	 * @returns {string}
	 */
	getPageLevelClass(page: Page): string {
		return `pnc-page-level-${page.pageLevel.toString()}`;
	}

	/**
	 * Show printable page format.
	 *
	 * @returns {void}
	 */
	printResult(): void {
		console.log('print...');
		// ...
		// ... TODO: show print page
		// ...
		window.open('/page/print');
	}

	/**
	 * Switch to selected page.
	 *
	 * @param {Page} page Page instance.
	 * @returns {void}
	 */
	openPage(page: Page): void {
		this.dataService.setCurrentPage(page);
	}

	/**
	 * On help button click handler.
	 * Dispatches the "onHelpClicked" event.
	 *
	 * @returns {void}
	 */
	showHelp(): void {
		this.helpClicked.emit();
	}

	/**
	 * Close menu click handler.
	 * Dispatches the "toggleMenu" event.
	 *
	 * @returns {void}
	 */
	closeMenu(): void {
		this.toggleMenu.emit();
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}
}
