import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-horizontal-bar',
	templateUrl: './horizontal-bar.component.html',
	styleUrls: ['./horizontal-bar.component.less']
})
export class HorizontalBarComponent implements OnInit {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	@Input()
	value = 0;

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}

}
