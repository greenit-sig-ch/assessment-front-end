import { Component, OnInit } from '@angular/core';

import { BasicPageTypeComponent } from '../basic-page-type.component';

import { Node } from '../../../vo/node';
import { Option, OptionType } from '../../../vo/option';
import { Page, PageType } from '../../../vo/page';
import { Question, QuestionType } from '../../../vo/question';

import { LoggerService, MODE } from '../../../service/logger.service';
import { DataService } from '../../../service/data.service';
import { HttpService } from '../../../service/http.service';
import { ConfigService } from '../../../service/config.service';

import { LocalizationPipe } from '../../../pipe/localization.pipe';
import { ToastrService } from 'ngx-toastr';

/**
 * Question results model.
 */
export class QuestionResult {

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * The maximum score for this question.
	 */
	get maxScore(): number {
		return this._maxScore;
	}

	/**
	 * The actual score for this question.
	 */
	get score(): number {
		return this._score;
	}

	/**
	 * The weight factor for this question.
	 */
	get weight(): number {
		return this._weight;
	}

	get stringValues(): Array<string> {
		return [this._score.toString(), this._maxScore.toString(), this.weight.toString()];
	}

	// -----------------------------
	//  Private properties
	// -----------------------------

	private _maxScore = 0;

	private _score = 0;

	private _weight = 0;

	/**
	 * Creates a new instance with initial values.
	 */
	constructor(aScore: number, aMaxScore: number, aWeight: number) {
		this._score = aScore;
		this._maxScore = aMaxScore;
		this._weight = aWeight;
	}
}

/**
 * Page results model.
 */
export class PageResult {

	/**
	 * Count of answered questions on this page or all subpages that are used
	 * to compute the score.
	 */
	answeredQuestionsWithScoreCount = 0;

	/**
	 * Count of answered questions on this page or all that are not used to compute the score.
	 */
	answeredQuestionsWithoutScoreCount = 0;

	/**
	 * Count of questions with score on this page that the user has answered with
	 * "not relevant".
	 */
	answeredQuestionsNotRelevantCount = 0;

	/**
	 * Count of questions on this page that are used to compute the score.
	 */
	questionsWithScoreCount = 0;

	/**
	 * Count of questions on this page that are not used to compute the score.
	 */
	questionsWithoutScoreCount = 0;

	/**
	 * If this page is a page that contains questions:
	 *   sumOfWeightedMaxScores = The sum of (question.maxScore * weight of the question on this page)
	 *   for all questions where question.hasScore == true and
	 *   the user has *NOT* selected the "not relevant" option.
	 *
	 * If this page is a page that has subpages:
	 *   sumOfWeightedMaxScores = The sum of (100 * weight of the question on this page)
	 *   for all questions where question.hasScore == true and
	 *   the user has *NOT* selected the "not relevant" option.
	 */
	sumOfWeightedMaxScores = 0;

	/**
	 * The sum of (value of the option selected by the user * weight of the question on this page)
	 * for all questions where question.hasScore == true and
	 * the user has *NOT* selected the "not relevant" option.
	 */
	sumOfWeightedValues = 0;

	/**
	 * The message to print out for this page.
	 */
	message = '';

	/**
	 * Returns the number of questions on this page or all subpages.
	 */
	get questionsCount(): number {
		return this.questionsWithScoreCount + this.questionsWithoutScoreCount;
	}

	/**
	 * Returns the number of answered questions on this page or all subpages.
	 */
	get answeredQuestionsCount(): number {
		return this.answeredQuestionsWithScoreCount + this.answeredQuestionsWithoutScoreCount;
	}

	/**
	 * Returns true if this page or all subpages contain at least one question
	 * with a score.
	 */
	get hasScore(): boolean {
		return this.questionsWithScoreCount > 0;
	}

	/**
	 * Returns true if the user has at least answered one question with a score
	 * on this page / all subpages with anything but "not relevant".
	 */
	get isRelevant(): boolean {
		return this.answeredQuestionsNotRelevantCount < this.questionsWithScoreCount;
	}

	/**
	 * Returns true if the user has answered "not relevant" to all questions
	 * with a score on this page / all subpages.
	 */
	get isNotRelevant(): boolean {
		return this.answeredQuestionsNotRelevantCount === this.questionsWithScoreCount;
	}

	/**
	 * Returns true if all questions on this page / all subpages have been answered.
	 */
	get allQuestionsAnswered(): boolean {
		return this.questionsCount === this.answeredQuestionsCount;
	}

	/**
	 * Returns the score value of this page as percentage of the maximum
	 * possible score of this page.
	 */
	get percentage(): number {
		return (this.sumOfWeightedMaxScores !== 0) ?
			Math.round(this.sumOfWeightedValues / this.sumOfWeightedMaxScores * 100) : 0;
	}
}

/**
 * Report panel model.
 */
export class ReportPanelModel {
	showDetailedEvaluation = true;
	isFullReport = false;
	showQuestionAdditionalInfo = false;
	showQuestionComment = false;
	showAllAnswers = false;
}

// -----------------------------
//  Component
// -----------------------------

/**
 * Evaluation component.
 */
@Component({
	selector: 'app-page-type-evaluation',
	templateUrl: './page-type-evaluation.component.html',
	styleUrls: ['./page-type-evaluation.component.less']
})
export class PageTypeEvaluationComponent extends BasicPageTypeComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------

	QuestionType = QuestionType;

	readonly ROOT_NODE = '__root';

	readonly panelModel: ReportPanelModel = new ReportPanelModel();

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Return questionnaire ID.
	 */
	get questionnaireId(): string {
		return this.dataService.getQuestionnaire().id;
	}

	// -----------------------------
	//  Private properties
	// -----------------------------

	/**
	 * Hashmap with question results.
	 */
	private questionResults: Map<string, QuestionResult> = new Map<string, QuestionResult>();

	/**
	 *  Hashmap with page results.
	 */
	private pageResults: Map<string, PageResult> = new Map<string, PageResult>();

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		readonly dataService: DataService,
		readonly httpService: HttpService,
		readonly logger: LoggerService,
		readonly configService: ConfigService,
		readonly lang: LocalizationPipe,
		readonly toastrService: ToastrService,
	) {
		super();
	}

	/**
	 *
	 */
	isChildPage(page: Page): boolean {
		return page.rootPage !== undefined;
	}

	/**
	 * Returns model PageResult with results.
	 *
	 * @param {string} pageId
	 * @returns {PageResult}
	 */
	getPageResult(pageId: string): PageResult {
		if (this.pageResults.has(pageId)) {
			return this.pageResults.get(pageId);
		}
		this.logger.log(MODE.error, `Results for page ${pageId} not found.`);

		return new PageResult();
	}

	/**
	 * Show page result if the node contain any questions with a score and
	 * if the node does have questions with a score but the user has not marked all of them as not relevant.
	 *
	 * @param {string} pageId
	 * @returns {boolean}
	 */
	showPageResult(pageId: string): boolean {
		const pr: PageResult = this.getPageResult(pageId);

		return pr.hasScore && !pr.isNotRelevant;
	}

	// public getRootPageResult(): PageResult {
	// 	if (this.pageResults.has(PageTypeEvaluationComponent.ROOT_NODE)) {
	// 		return this.pageResults.get(PageTypeEvaluationComponent.ROOT_NODE)
	// 	}
	// 	this.logger.log(MODE.error, `Results for root page not found.`);
	// 	return new PageResult();
	// }
	//
	// /**
	//  * Return the question number.
	//  *
	//  * @param {Page} page instance.
	//  * @param {Question} question Question instance.
	//  * @return {string} String with question number like a "X.Y.Z"
	//  */
	// public getQuestionNumber(page: Page, question: Question): string {
	// 	return '.' + (this.questionsForPage(page).indexOf(question) + 1).toString();
	// }
	//
	// /**
	//  * Return the question comment.
	//  *
	//  * @param {Question} question The Question instance.
	//  * @return {string} Text from <b>Answer</b> model from question instance.
	//  */
	// public getQuestionComment(question: Question): string {
	// 	return question.answer.comment;
	// }
	//
	// /**
	//  * Return selected option from question.
	//  * @param question Is Question instance.
	//  * @return Text of selected option from <b>Answer</b> model from question instance.
	//  */
	// public getSelectedOption(question: Question): string {
	// 	let result = '';
	// 	question.options.forEach(option => {
	// 		if (question.answer.selection.get(option.id)) {
	// 			result = result + this.lang.transform('title', option.id) + '<br>';
	// 			this.logger.log(MODE.log, option);
	// 		}
	// 	});
	//
	// 	return result;
	// }

	/**
	 * Specifies how display this page.
	 * For child pages will use different statistics.
	 */
	isPageReadyToShow(page: Page): boolean {
		return page.printSummary > 0;
	}

	getPercentageArray(pageId: string): Array<number> {
		const percentage: number = this.getPercentage(pageId);

		return [percentage];
	}

	getPercentage(pageId: string): number {
		if (this.pageResults.has(pageId)) {
			const result: PageResult = this.pageResults.get(pageId);

			return result.percentage;
		}

		return 0;
	}

	getAnsweredQuestions(pageId: string): Array<number> {
		if (this.pageResults.has(pageId)) {
			const result: PageResult = this.pageResults.get(pageId);
			const a: number = result.answeredQuestionsCount;
			const b: number = result.questionsCount;
			const c: number = result.answeredQuestionsNotRelevantCount;

			return [a, b, c];
		}

		return [0, 0, 0];
	}

	getQuestionScore(page: Page, question: Question): Array<number> {
		const id = `${page.id}:${question.id}`;
		if (this.questionResults.has(id)) {
			const result: QuestionResult = this.questionResults.get(id);
			const a: number = result.score;
			const b: number = result.maxScore;

			return [a, b];
		}

		return [0, 0];
	}

	getQuestionPercentage(page: Page, question: Question): number {
		const id = `${page.id}:${question.id}`;
		if (this.questionResults.has(id)) {
			const result: QuestionResult = this.questionResults.get(id);

			return Math.round(result.score / result.maxScore * 100);
		}

		return 0;
	}

	/**
	 * Return style number by percent.
	 *
	 * @param {number} percentage
	 * @returns {number}
	 */
	percentColor(percentage: number): number {
		if (percentage >= 0 && percentage < 20) {
			return 1;
		} else if (percentage >= 20 && percentage < 40) {
			return 2;
		} else if (percentage >= 40 && percentage < 60) {
			return 3;
		} else if (percentage >= 60 && percentage < 80) {
			return 4;
		} else if (percentage >= 80 && percentage <= 100) {
			return 5;
		}

		return 0;
	}

	/**
	 * Detect visibility of page inside report container.
	 * If the user selects Management summary, then display all pages (and subpages) where
	 * the printSummary property is > 0. If the user selects Full report, then print all pages
	 * (and subpages) where the printDetailed property is > 0.
	 *
	 * @param {Page} page
	 * @returns {boolean}
	 */
	isVisibleDetailedPage(page: Page): boolean {
		return (page.printSummary && !this.panelModel.isFullReport)
			|| (page.printDetailed && this.panelModel.isFullReport);
	}

	/**
	 * Detect visibility of question additional information.
	 *
	 * @param {Question} question
	 * @returns {boolean}
	 */
	isVisibleQuestionAdditionalInfo(question: Question): boolean {
		return this.panelModel.showQuestionAdditionalInfo
			&& this.configService.getConfiguration().langPackHashMap
				.has(`${question.id}-description`)
			&& (question.type === QuestionType[QuestionType.singleChoice]
				|| question.type === QuestionType[QuestionType.multipleChoice]
				|| question.type === QuestionType[QuestionType.dropDown]);
	}

	/**
	 * Detect visibility of question footer.
	 *
	 * @param {Question} question
	 * @param {Page} page
	 * @returns {boolean}
	 */
	isVisibleQuestionFooter(page: Page, question: Question): boolean {
		const questionScore = this.questionResults.get(`${page.id}:${question.id}`);

		return (question.answer.selection.has('not-relevant') || questionScore)
			&& (question.type === QuestionType[QuestionType.singleChoice]
				|| question.type === QuestionType[QuestionType.multipleChoice]
				|| question.type === QuestionType[QuestionType.dropDown]);
	}

	/**
	 * Detect visibility of question comment.
	 *
	 * @param {Question} question
	 * @returns {boolean}
	 */
	isVisibleQuestionComment(question: Question): boolean {
		return this.panelModel.showQuestionComment
			&& question.answer.comment !== undefined
			&& question.answer.comment.length > 0;
	}

	/**
	 * Display all options for single- and multiple-choice questions.
	 * If it is not checked, then only the option(s) selected by the user is displayed.
	 *
	 * @param {Question} question
	 * @param {Option} option
	 * @returns {boolean}
	 */
	isVisibleAnswer(question: Question, option: Option): boolean  {
		return this.panelModel.showAllAnswers
			|| (!this.panelModel.showAllAnswers && question.answer.selection.get(option.id));
	}

	getQuestionFooter(page: Page, question: Question): string {
		const nr = 'not-relevant';
		if (question.answer.selection.get(nr)) {
			return this.lang.transform('QuestionIsntRelevantNoScore');
		} else {
			const questionScore = this.questionResults.get(`${page.id}:${question.id}`);
			if (questionScore) {
				return this.lang.transform('QuestionFooter', undefined, questionScore.stringValues);
			} else {
				return '';
			}
		}
	}

	/**
	 * Return selected value for dropdown component.
	 *
	 * @param {Question} question
	 * @returns {string}
	 */
	getDropdownValue(question: Question): string {
		let result = '';
		question.answer.selection.forEach((v, k) => {
			if (v) {
				result = k;
			}
		});

		return result;
	}

	/**
	 * Return true it it's singleChoice, multipleChoice or dropDown.
	 *
	 * @param {string} type
	 * @returns {boolean}
	 */
	isChoiceComponent(type: string): boolean {
		return type === QuestionType[QuestionType.singleChoice]
			|| type === QuestionType[QuestionType.multipleChoice]
			|| type === QuestionType[QuestionType.dropDown];
	}

	// -----------------------------
	//  Calculation functions
	// -----------------------------

	/**
	 * Calculate results for current questionnaire.
	 * This function will change <b>Result</b> model.
	 *
	 * @return {void}
	 */
	private calculateResults(): void {

		const rootNode: Node = new Node();

		rootNode.rid = this.ROOT_NODE;
		rootNode.children = this.dataService.getQuestionnaire().structure;
		this.calculateNodeResult(rootNode);
	}

	/**
	 * Recursive function to calculate the result for all pages of type "question".
	 *
	 * @param node
	 */
	private calculateNodeResult(node: Node): void {

		let result: PageResult;
		let page: Page;

		if (node.rid !== this.ROOT_NODE) {
			page = this.dataService.getPageForId(node.rid);
		} else {
			page = new Page();
			page.id = node.rid;
			page.type = PageType[PageType.question];
		}

		if (node.children && node.children.length > 0) {
			// The current node is not a leaf node --> Descend further.
			node.children.forEach(childNode => {
				this.calculateNodeResult(childNode);
			});
			// When the score of all child nodes has been calculated, then
			// calculate the score for this node.
			result = this.calculateResultFromChildPages(node, page);
		} else {
			// This is a leaf node --> If it is of type "question", then
			// calculate the results from the questions.
			if (page.type === PageType[PageType.question]) {
				result = this.calculateResultFromQuestions(page);
			} else {
				this.logger.log(MODE.info, `Skipped page ${page.id}`);
			}
		}

		// Add the page result to the list of results.
		if (page.type === PageType[PageType.question]) {
			this.pageResults.set(page.id, result);
			this.logResult(page.id, result);
		}
	}

	/**
	 * Calculates the result for pages that do not contain questions but have child pages.
	 *
	 * @param {Node} node The node for which to calculate the result
	 * @param {Page} page The page instance for the node
	 * @returns{PageResult} The result computed for this node
	 */
	private calculateResultFromChildPages(node: Node, page: Page): PageResult {
		const pageResult: PageResult = new PageResult();

		node.children.forEach(childNode => {
			const pageId: string = childNode.rid;

			// Only consider question child pages
			if (this.dataService.getPageForId(pageId).type === PageType[PageType.question]) {
				if (this.pageResults.has(pageId)) {
					const childResult: PageResult = this.pageResults.get(pageId);

					// Add the question counts
					pageResult.answeredQuestionsWithScoreCount += childResult.answeredQuestionsWithScoreCount;
					pageResult.answeredQuestionsWithoutScoreCount += childResult.answeredQuestionsWithoutScoreCount;
					pageResult.answeredQuestionsNotRelevantCount += childResult.answeredQuestionsNotRelevantCount;
					pageResult.questionsWithScoreCount += childResult.questionsWithScoreCount;
					pageResult.questionsWithoutScoreCount += childResult.questionsWithoutScoreCount;

					// Only take the page into account if it is relevant
					if (childResult.isRelevant) {
						// The maximum score of a child page is always 100 (percent)
						pageResult.sumOfWeightedMaxScores += childNode.weight * 100;
						pageResult.sumOfWeightedValues += childResult.percentage * childNode.weight;
					}
				} else {
					// Should not happen if the recursive function has been implemented correctly...
					this.logger.log(MODE.error, `Could not find pageResult for page ${pageId}`);
				}
			}
		});

		return pageResult;
	}

	/**
	 * Calculates the result for pages that do not contain questions but have child pages.
	 *
	 * @param {Page} page The page for which to calculate the result
	 * @returns {PageResult} The result computed for this page
	 */
	private calculateResultFromQuestions(page: Page): PageResult {
		let question: Question;
		const pageResult: PageResult = new PageResult();

		// parse page questions
		page.contents.forEach(pageContent => {

			// get question instance
			question = this.dataService.getQuestionHashmap()
				.get(pageContent.rid);
			if (question) {

				if (question.hasScore) {

					// count questions with score
					pageResult.questionsWithScoreCount++;

					// count questions with score and answers
					if (question.hasAnswer) {
						pageResult.answeredQuestionsWithScoreCount++;
					}

					if (question.type === QuestionType[QuestionType.multipleChoice]
						|| question.type === QuestionType[QuestionType.singleChoice]) {

						// count questions with NR answer
						const option: Option = question.options.find(o => o.type === OptionType[OptionType.nr]);
						if (option
							&& question.answer.selection.has(option.id)
							&& question.answer.selection.get(option.id)) {
							pageResult.answeredQuestionsNotRelevantCount++;
						}

						const weightedScore = this.dataService.getScoreForQuestion(question) * pageContent.weight;
						const weightedMaxScore = this.dataService.getMaxScoreForQuestion(question) * pageContent.weight;
						pageResult.sumOfWeightedValues += weightedScore;
						pageResult.sumOfWeightedMaxScores += weightedMaxScore;

						// Save this question's result.
						const questionResult = new QuestionResult(
							this.dataService.getScoreForQuestion(question),
							question.maxScore,
							pageContent.weight
						);
						this.questionResults.set(`${page.id}:${question.id}`, questionResult);

					}
				} else {
					// The current question does not have a score
					pageResult.questionsWithoutScoreCount++;
					if (question.hasAnswer) {
						pageResult.answeredQuestionsWithoutScoreCount++;
					}
				}
			}
		});

		return pageResult;
	}

	private logResult(pageId: string, pageResult: PageResult): void {
		this.logger.log(MODE.info, `Calculated values for page ${pageId}`);
		this.logger.log(MODE.info, `  # questions (with / without score) = ${pageResult.questionsCount} `
			+ `(${pageResult.questionsWithScoreCount} / ${pageResult.questionsWithoutScoreCount})`);
		this.logger.log(MODE.info, `  # answered questions (with / without score) = `
			+ `${pageResult.answeredQuestionsCount} (${pageResult.answeredQuestionsWithScoreCount} / `
			+ `${pageResult.answeredQuestionsWithoutScoreCount})`);
		this.logger.log(MODE.info, `  # questions answered with "not relevant" = `
			+ `${pageResult.answeredQuestionsNotRelevantCount}`);
		this.logger.log(MODE.info, `  --> page is relevant: ${!pageResult.isNotRelevant}`);
		this.logger.log(MODE.info, `  --> page score = ${pageResult.sumOfWeightedValues} out of `
			+ `${pageResult.sumOfWeightedMaxScores} = ${pageResult.percentage}%`);
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
		this.calculateResults();
	}
}
