import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {PageTypeEvaluationComponent} from './page-type-evaluation.component';

describe('PageTypeEvaluationComponent', () => {
	let component: PageTypeEvaluationComponent;
	let fixture: ComponentFixture<PageTypeEvaluationComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PageTypeEvaluationComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PageTypeEvaluationComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
