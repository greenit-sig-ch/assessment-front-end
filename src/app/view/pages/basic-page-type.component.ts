import { Component } from '@angular/core';

import { ConfigService } from '../../service/config.service';
import { DataService } from '../../service/data.service';
import { HttpService } from '../../service/http.service';
import { LoggerService } from '../../service/logger.service';

import { LocalizationPipe } from '../../pipe/localization.pipe';

import { Page } from '../../vo/page';
import { Question } from '../../vo/question';
import { ToastrService } from 'ngx-toastr';

/**
 * Basic page type component.
 */
@Component({
	template: ''
})
export abstract class BasicPageTypeComponent {

	abstract readonly dataService: DataService;
	abstract readonly httpService: HttpService;
	abstract readonly logger: LoggerService;
	abstract readonly configService: ConfigService;
	abstract readonly lang: LocalizationPipe;
	abstract readonly toastrService: ToastrService;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Get questions array for selected page.
	 *
	 * @returns {Array<Question>}
	 */
	get questionsForCurrentPage(): Array<Question> {
		return this.questionsForPage(this.dataService.getCurrentPage());
	}

	/**
	 * Return questions list for corresponding page.
	 *
	 * @param {Page} page
	 * @returns {Array<Question>}
	 */
	questionsForPage(page: Page): Array<Question> {
		const result: Array<Question> = [];
		if (page) {
			if (Array.isArray(page.contents) && page.contents.length > 0) {
				page.contents.forEach(pageContent => {
					if (this.dataService.getQuestionHashmap().has(pageContent.rid)) {
						result.push(this.dataService.getQuestionHashmap().get(pageContent.rid));
					}
				});
			}
		}

		return result;
	}

	/**
	 * Return structured page list.
	 *
	 * @returns {Array<Page>}
	 */
	get pages(): Array<Page> {
		return this.dataService.getStructuredPages();
	}

}
