import { Component, Input, OnInit } from '@angular/core';

import { LocalizationPipe } from '../../../pipe/localization.pipe';
import { ConfigService } from '../../../service/config.service';
import { DataService } from '../../../service/data.service';
import { Page } from '../../../vo/page';

/**
 * Page title component.
 */
@Component({
	selector: 'app-page-title',
	templateUrl: 'page-title.component.html',
	styleUrls: ['./page-title.component.less']
})
export class PageTitleComponent implements OnInit {

	// -----------------------------
	//  Input properties
	// -----------------------------

	/**
	 * Current page.
	 *
	 * @param {Page} value
	 */
	@Input()
	set currentPage(value: Page) {
		this._currentPage = value;
		this._description = this.lang.transform('description', this.dataService.getCurrentPage().id);
		this._showDescription = this._description && this._description.length > 0;
	}
	get currentPage(): Page {
		return this._currentPage;
	}

	// -----------------------------
	//  Getters and setters
	// ----------------------------

	/**
	 * Return the text of description in case if it present for the current page.
	 *
	 * @returns {string}
	 */
	get description(): string {
		return this._description;
	}

	/**
	 * Flag which detect page description number visibility.
	 *
	 * @returns {boolean}
	 */
	get showDescription(): boolean {
		return this._showDescription;
	}

	/**
	 * Flag which detect page number visibility.
	 */
	get showPageNumber(): boolean {
		return this.configService.getConfiguration().showPageNumber;
	}

	// -----------------------------
	//  Private properties
	// -----------------------------

	private _currentPage: Page;
	private _showDescription = false;
	private _description: string;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of PageTitleComponent.
	 *
	 * @param {ConfigService} configService
	 * @param {DataService} dataService
	 * @param {LocalizationPipe} lang
	 */
	constructor(
		private readonly configService: ConfigService,
		private readonly dataService: DataService,
		private readonly lang: LocalizationPipe
	) {}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
	}
}
