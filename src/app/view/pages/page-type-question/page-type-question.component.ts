import { Component, OnInit } from '@angular/core';

import { BasicPageTypeComponent } from '../basic-page-type.component';

import { ConfigService } from '../../../service/config.service';
import { DataService } from '../../../service/data.service';
import { HttpService } from '../../../service/http.service';
import { LoggerService } from '../../../service/logger.service';

import { LocalizationPipe } from '../../../pipe/localization.pipe';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-page-type-question',
	templateUrl: './page-type-question.component.html',
	styleUrls: ['./page-type-question.component.less']
})
export class PageTypeQuestionComponent extends BasicPageTypeComponent implements OnInit {

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		readonly dataService: DataService,
		readonly httpService: HttpService,
		readonly logger: LoggerService,
		readonly configService: ConfigService,
		readonly lang: LocalizationPipe,
		readonly toastrService: ToastrService,
	) {
		super();
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}

}
