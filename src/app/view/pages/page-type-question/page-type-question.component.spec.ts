import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {PageTypeQuestionComponent} from './page-type-question.component';

describe('PageTypeQuestionComponent', () => {
	let component: PageTypeQuestionComponent;
	let fixture: ComponentFixture<PageTypeQuestionComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PageTypeQuestionComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PageTypeQuestionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
