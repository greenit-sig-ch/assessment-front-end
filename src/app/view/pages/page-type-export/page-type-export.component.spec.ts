import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {PageTypeExportComponent} from './page-type-export.component';

describe('PageTypeExportComponent', () => {
	let component: PageTypeExportComponent;
	let fixture: ComponentFixture<PageTypeExportComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PageTypeExportComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PageTypeExportComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
