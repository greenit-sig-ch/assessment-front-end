import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { DataService } from '../../../../service/data.service';

import { Answer } from '../../../../vo/answer';

@Component({
	selector: 'app-page-import-modal-alert',
	templateUrl: './page-import-modal-alert.component.html',
	styleUrls: ['./page-import-modal-alert.component.less']
})
export class PageImportModalAlertComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------

	@ViewChild('autoShownModal')
	autoShownModal: ModalDirective;

	isModalShown = false;

	// -----------------------------
	//  Private properties
	// -----------------------------

	private answers: Array<Answer>;

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		private readonly dataService: DataService
	) {}

	/**
	 * Show modal alert window.
	 * @param {Array<Answer>} answers Answer object for parser.
	 */
	showModal(answers: Array<Answer>): void {
		this.answers = answers;
		this.isModalShown = true;
	}

	/**
	 * Hide modal alert window.
	 */
	hideModal(): void {
		this.autoShownModal.hide();
	}

	/**
	 * When alert disappeared.
	 */
	onHidden(): void {
		this.isModalShown = false;
	}

	/**
	 * Cancel import.
	 */
	cancelImport(): void {
		this.hideModal();
		this.answers = undefined;
	}

	/**
	 * Agree import file.
	 */
	public continueImport(): void {
		this.hideModal();
		this.dataService.setAnswersToQuestionnaire(this.answers);
	}

	// -----------------------------
	//  Lifecicle functions
	// -----------------------------

	ngOnInit(): void {}

}
