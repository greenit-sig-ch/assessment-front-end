import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {PageImportModalAlertComponent} from './page-import-modal-alert.component';

describe('PageExportModalAlertComponent', () => {
	let component: PageImportModalAlertComponent;
	let fixture: ComponentFixture<PageImportModalAlertComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PageImportModalAlertComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PageImportModalAlertComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
