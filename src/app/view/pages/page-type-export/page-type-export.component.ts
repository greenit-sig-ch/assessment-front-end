import { Component, OnInit, ViewChild } from '@angular/core';

import { HttpService, QUESTIONNAIRE } from '../../../service/http.service';
import { LoggerService, MODE } from '../../../service/logger.service';

import { BasicPageTypeComponent } from '../basic-page-type.component';
import { PageImportModalAlertComponent } from './page-import-modal-alert/page-import-modal-alert.component';

import { Answer } from '../../../vo/answer';
import { SaveAnswer } from '../../../vo/save-answer';

import { DataService } from '../../../service/data.service';
import { ConfigService } from '../../../service/config.service';

import { LocalizationPipe } from '../../../pipe/localization.pipe';
import { ToastrService } from 'ngx-toastr';

// -----------------------------
//  Constants
// -----------------------------

/**
 * Check object public properties.
 *
 * @param testObject
 * @returns {boolean}
 */
const checkAnswerProperties = (testObject: any): boolean => {
	const answer: Answer = new Answer();
	for (const key in answer) {
		if (answer.hasOwnProperty(key) && typeof key !== 'function') {
			// is a public property
			if (testObject[key] === undefined) {
				return false;
			}
		}
	}

	return true;
};

@Component({
	selector: 'app-page-type-export',
	templateUrl: './page-type-export.component.html',
	styleUrls: ['./page-type-export.component.less'],
})
export class PageTypeExportComponent extends BasicPageTypeComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------

	@ViewChild(PageImportModalAlertComponent, {static: true})
	importAlert: PageImportModalAlertComponent;

	// -----------------------------
	//  Private properties
	// -----------------------------

	private fileList: any;
	private fileName: string;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	get hasFileReader(): boolean {
		return window.hasOwnProperty('FileReader');
	}

	get defaultPath(): string {
		if (this.configService.environment.isMac) {
			return '~/Downloads';
		} else if (this.configService.environment.isLinux) {
			return '/home/<username>/Downloads';
		} else if (this.configService.environment.isWindows) {
			return 'Downloads';
		}

		return undefined;
	}

	get isImportDisabled(): boolean {
		return !this.fileList;
	}

	get selectedFileName(): string {
		return this.fileName;
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		readonly dataService: DataService,
		readonly httpService: HttpService,
		readonly logger: LoggerService,
		readonly configService: ConfigService,
		readonly lang: LocalizationPipe,
		readonly toastrService: ToastrService,
	) {
		super();
	}

	/**
	 * Download answers to local computer.
	 */
	download(): void {
		const saveAnswer: SaveAnswer = new SaveAnswer(
			this.dataService.getQuestionnaire(),
			this.dataService.getQuestionnaireHash(),
			this.dataService.getUUID());
		this.httpService.downloadFile(JSON.stringify(saveAnswer), QUESTIONNAIRE);
	}

	/**
	 * Read and check file for correct format.
	 */
	onSubmit(event: any): void {
		// read import file
		const file = this.fileList[0];
		const fileReader = new FileReader();
		fileReader.onloadend = (e: any) => {
			try {
				const saveAnswer: SaveAnswer = JSON.parse(fileReader.result as string);

				// check model
				if (saveAnswer.qid === this.dataService.getQuestionnaire().id
					&& saveAnswer.version === this.dataService.getQuestionnaire().version
					&& saveAnswer.answers instanceof Array
					&& saveAnswer.answers.length
					&& checkAnswerProperties(saveAnswer['answers'][0])) {

					// collect answers to array of Answer
					const answers: Array<Answer> = new Array<Answer>();
					(saveAnswer.answers as Array<any>).forEach(o => answers.push(Answer.fromSimpleObject(o)));
					this.importAlert.showModal(answers);
				} else {
					this.toastrService.warning(
						this.lang.transform('Wrong file format'),
						this.lang.transform('Import')
					);
				}
			} catch (err) {
				this.toastrService.warning(
					this.lang.transform('Wrong file type'),
					this.lang.transform('Import')
				);
			}
		};
		fileReader.readAsText(file);
	}

	/**
	 * File is selected and ready for import.
	 */
	onChange(event: any): void {
		this.logger.log(MODE.log, event);
		
		if (event.target.files?.length > 0) {
			this.fileList = event.target.files;
			this.fileName = this.fileList[0]?.name;
		}
	}

	// -----------------------------
	//  Private functions
	// -----------------------------

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
		this.fileName = '';
	}

}
