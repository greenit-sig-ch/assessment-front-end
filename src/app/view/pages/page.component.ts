import { Component, OnInit } from '@angular/core';

import { ConfigService } from '../../service/config.service';
import { DataService } from '../../service/data.service';
import { LoggerService, MODE } from '../../service/logger.service';
import { Page, PageType } from '../../vo/page';

enum Direction {
	PrevPage = -1,
	NextPage = 1
}

/**
 * Page component.
 * Displays the current active page and questions for this page.
 */
@Component({
	selector: 'app-page',
	templateUrl: 'page.component.html',
	styleUrls: ['./page.component.less']
})
export class PageComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------

	readonly Direction = Direction;
	readonly PageType = PageType;

	// -----------------------------
	//  Getters and setters
	// -----------------------------

	/**
	 * Return next page level for <b>Next Page</b> button.
	 *
	 * @returns {string}
	 */
	get nextPageLabel(): string {
		return this.dataService.getCurrentPage().nextPageLabel ?
			this.dataService.getCurrentPage().nextPageLabel :
			'NextPage';
	}

	/**
	 * Show previous page button.
	 *
	 * @returns {boolean}
	 */
	get showPrevButton(): boolean {
		return this.dataService.getCurrentPageIndex() > 1;
	}

	/**
	 * Show next page button.
	 *
	 * @returns {boolean}
	 */
	get showNextButton(): boolean {
		return this.dataService.getCurrentPageIndex() < this.dataService.getStructuredPages().length;
	}

	/**
	 * Return current page type.
	 *
	 * @returns {string}
	 */
	get currentPageType(): string {
		return this.dataService.getCurrentPage().type;
	}

	/**
	 * Return current page type.
	 *
	 * @returns {string}
	 */
	get currentPageId(): string {
		return this.dataService.getCurrentPage().id;
	}

	/**
	 * Return current page number.
	 *
	 * @returns {number}
	 */
	get currentPageNumber(): number {
		let result = 1;
		this.dataService.getStructuredPages()
			.some(p => {
				if (p.type === PageType[PageType.question] && p !== this.dataService.getCurrentPage()) {
					result++;

					return false;
				}

				return true;
			});

		return result;
	}

	/**
	 * Return total page numbers.
	 */
	get totalPages(): number {
		return this.dataService.getStructuredPages()
			.filter(p => p.type === PageType[PageType.question]).length;
	}

	/**
	 * Indicates that page navigator is visible.
	 *
	 * @returns {boolean}
	 */
	get showPageCounter(): boolean {
		return (! this.configService.getConfiguration().showPageNavigator)
			&& this.dataService.getCurrentPage().type === PageType[PageType.question];
	}

	// -----------------------------
	//  Public functions
	// -----------------------------

	constructor(
		private readonly dataService: DataService,
		private readonly logger: LoggerService,
		private readonly configService: ConfigService
	) {}

	/**
	 * Switch to next/previous page or subpage.
	 *
	 * @param direction Switching direction.
	 * @returns {void}
	 */
	switchPage(direction: number): void {
		const arr: Array<Page> = this.dataService.getStructuredPages();
		const cp: Page = this.dataService.getCurrentPage();

		// check for 'nextPageId' property
		if (direction === Direction.NextPage && cp.nextPageId && cp.nextPageId.length > 0) {
			const op: Array<Page> = arr.filter(p => p.id === cp.nextPageId);
			if (op && op.length) {
				this.logger.log(MODE.log, 'Found <nextPageId> property! Next page will be:', op[0].id);
				this.dataService.setPreviousPage(cp);
				this.dataService.setCurrentPage(op[0]);

				return;
			}
		}

		if (direction === Direction.PrevPage && this.dataService.getPreviousPage()) {
			this.dataService.setCurrentPage(this.dataService.getPreviousPage());
			this.dataService.setPreviousPage(null);
		} else {
			const idx: number = this.dataService.getCurrentPageIndex() - 1 + direction;
			this.dataService.setPreviousPage(null);

			// index is ok?
			if (idx >= 0 && idx < arr.length) {
				// check for mandatory
				const newPage: Page = this.dataService.getStructuredPages()[idx];
				if (this.dataService.checkForMandatoryPageAndShowAlert(newPage)) {
					this.dataService.setCurrentPage(newPage);
				}
			}
		}
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {}
}
