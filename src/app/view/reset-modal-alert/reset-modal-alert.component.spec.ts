import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {ResetModalAlertComponent} from './reset-modal-alert.component';

describe('ResetModalAlertComponent', () => {
	let component: ResetModalAlertComponent;
	let fixture: ComponentFixture<ResetModalAlertComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ResetModalAlertComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ResetModalAlertComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
