import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
	selector: 'app-reset-modal-alert',
	templateUrl: './reset-modal-alert.component.html',
	styleUrls: ['./reset-modal-alert.component.less']
})
export class ResetModalAlertComponent implements OnInit {

	// -----------------------------
	//  Public properties
	// -----------------------------

	isModalShown = false;

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of ResetModalAlertComponent.
	 *
	 * @param {DataService} dataService
	 */
	constructor(
		private readonly dataService: DataService
	) {}

	/**
	 * Reset questions and save configuration.
	 */
	resetQuestions(): void {
		this.isModalShown = false;

		this.dataService.resetQuestionnaire();
		this.dataService.saveCurrentAnswers();
		this.dataService.callHandlerOnInit();
	}

	/**
	 * Cancel reset questions.
	 *
	 * @returns {void}
	 */
	cancelResetQuestions(): void {
		this.isModalShown = false;
	}

	// -----------------------------
	//  Lifecycle functions
	// -----------------------------

	ngOnInit(): void {
	}
}
