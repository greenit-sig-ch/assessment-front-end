import { Pipe, PipeTransform } from '@angular/core';
import { ConfigService } from '../service/config.service';

/**
 * Localization pipe class.
 * Function <b>transform</b> will try to localize string resources.
 */
@Pipe({
	name: 'lang',
	pure: false
})
export class LocalizationPipe implements PipeTransform {

	// -----------------------------
	//  Public functions
	// -----------------------------

	/**
	 * Creates an instance of LocalizationPipe.
	 */
	constructor(
		private readonly configService: ConfigService
	) {}

	/**
	 * String transformer function.
	 * Return string resource by key <b>id-strKey</b> if or just by <b>strKey</b> if id is undefined.
	 *
	 * @param {string} strKey
	 * @param {string} id
	 * @param {Array<string>} prop
	 * @returns {string}
	 */
	transform(strKey: string, id?: string, prop?: Array<string | number>): string {
		const key: string = (id ? `${id}-${strKey}` : strKey);
		if (strKey
				&& this.configService.getConfiguration()
				&& this.configService.getConfiguration().langPackHashMap
				&& this.configService.getConfiguration().langPackHashMap
					.has(key)
		) {
			let result: string = this.configService.getConfiguration().langPackHashMap
				.get(key);
			if (prop !== undefined) {
				const rma: RegExpMatchArray = result.match(/{{[0-9a-z\sA-Z]*}}/gi);
				rma.forEach((v, idx) => result = result.replace(v, `${prop[idx]}`));
			}

			return result;
		}

		return this.configService.getConfiguration().showUndefinedStrings ? `%${key}%` : '';
	}

	/**
	 * Checks the availability of the resource.
	 *
	 * @param {string} strKey
	 * @returns {boolean} Return <b>true</b> if resource exists or <b>showUndefinedStrings</b> is set to <b>true</b>.
	 */
	checkResourceId(strKey: string): boolean {
		return this.configService.getConfiguration().langPackHashMap
				.has(strKey)
			|| this.configService.getConfiguration().showUndefinedStrings;
	}
}
