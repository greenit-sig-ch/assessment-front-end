/***********************************************************************************************************************
 * Custom handlers package.
 *
 */

window.questionnaireFunctions = {};

/**********************************************************************************************************************/

/**
 * Function that is called when the user opens a new assessment page
 *
 * @param {Configuration} configuration --> The application configuration (from config.yaml)
 * @param {Questionnaire} questionnaire --> The complete questionnaire
 * @param {Page} currentPage --> The page that the user has opened at the moment
 * @param {Page} newPage --> The page that will be opened next
 * @returns {void}
 */
window.questionnaireFunctions.handlerOnPageLoad = function (configuration, questionnaire, currentPage, newPage) {

};


/**
 * Function that is called when the user has selected an answer for a question
 *
 * @param {Configuration} configuration --> The application configuration (from config.yaml)
 * @param {Questionnaire} questionnaire --> The complete questionnaire
 * @param {Page} page --> The current page
 * @param {Question} question --> The current question
 * @param {Option} option --> The option that the user has selected
 * @returns {void}
 */
window.questionnaireFunctions.answerGiven = function (configuration, questionnaire, page, question, option) {

};
