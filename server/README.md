# Server directory

This directory contains the server-side PHP code that can handle tracking requests from the assessment application.

## Prerequisites

- MySQL database
- Webserver with PHP support

## Installation

1. Create a MySQL database on the server.
2. Edit **RestAPI/Config.inc.php** and enter the connection details for the MySQL database.
3. Copy the **RestAPI** directory to the server, including the **.htaccess** file contained in that directory.

The necessary database table is created automatically when the first tracking request arrives.

## Enable tracking in the assessment front-end

In order to enable tracking in the assessment app, the configuration file **src/content/config.yaml** must be edited:

1. Set the *trackingMode* property to a value between 1 and 5 (0 disables the tracking):

    * **1** → Only page open events are tracked
    * **2** → Track which questions have been answered but not the answer itself
    * **3** → Track which questions have been answered and also transmit the answer
    * **4** → Track all user activity

1. Specify the URL to access the RestAPI folder with the *restApiUrl* property.
