CREATE TABLE IF NOT EXISTS `tracking` (
		`id` int unsigned NOT NULL AUTO_INCREMENT,
		`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		`event_type` enum('pageOpened','answerGiven') NOT NULL,
		`uuid` varchar(50) NOT NULL,
		`assessment_id` varchar(200) NOT NULL,
		`assessment_version` int unsigned NOT NULL,
		`page_id` varchar(200) NOT NULL,
		`page_version` int unsigned NOT NULL,
		`question_id` varchar(200) DEFAULT NULL,
		`question_version` int unsigned DEFAULT NULL,
		`option_id` varchar(200) DEFAULT NULL,
		`option_text` text DEFAULT NULL,
		`option_comment` text DEFAULT NULL,
		`language` varchar(10) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `id_UNIQUE` (`id`)
) DEFAULT CHARSET=utf8;
