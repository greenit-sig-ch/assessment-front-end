INSERT INTO `tracking` (
	`event_type`,
	`uuid`,
	`assessment_id`,
	`assessment_version`,
	`page_id`,
	`page_version`,
	`question_id`,
	`question_version`,
	`option_id`,
	`option_text`,
	`option_comment`,
	`language` )
VALUES (?,?,?,?,?,?,?,?,?,?,?,?);
