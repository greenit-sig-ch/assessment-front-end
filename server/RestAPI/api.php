<?php

	require_once("Rest.inc.php");

	class API extends REST 
	{
		// -----------------------------
		//  Private properties
		// -----------------------------
		
		private $mysqli = NULL;
		
		// -----------------------------
		//  Public functions
		// -----------------------------
		
		/**
		 * Constructor.
		 * Open DB connection.
		 */
		public function __construct() 
		{
			parent::__construct();
			$this->dbConnect();
		}
		
		/**
		 * Destructor.
		 * Close DB connection.
		 */
		public function __destruct()
		{
			if ( $this->mysqli )
			{
				$this->mysqli->close();
			}
		}

		/**
		 * Public method for access api.
		 * This method dynmically call the method based on the query string.
		 */
		public function processApi() 
		{
			if ( is_array($_REQUEST) && count($_REQUEST) >= 1 && $_REQUEST['rquest'] )
			{
				$func = strtolower( trim(str_replace("/", "", $_REQUEST['rquest'])) );
				if ( (int) method_exists($this, $func) > 0 && in_array( $func, $this->_allow ) )
				{
					$this->$func();
					return;
				}
			}
			
			$this->response( [], 404 );
		}
		
		// -----------------------------
		//  Private functions
		// -----------------------------
		
		/**
		 * Connect to DB.
		 */
		private function dbConnect()
		{
			$conf = include_once "Config.inc.php";
			$this->mysqli = new mysqli( $conf["host"], $conf["user"], $conf["password"], $conf["database"], $conf["port"] );
			if ( $this->mysqli->connect_errno )
			{
				self::init($conf);
			}
			else
			{
				self::setDbCharset();
				
				$result = $this->mysqli->query("SHOW TABLES LIKE 'tracking'");
				if ( $result->num_rows == 0 )
				{
					self::createTables();
				}
			}
		}
		
		/**
		 * Initialize DB.
		 */
		private function init( $conf )
		{
			$this->mysqli = new mysqli( $conf["host"], $conf["user"], $conf["password"], null, $conf["port"]);
			if ( $this->mysqli->connect_errno )
			{
				printf( "Can't connect to MySQL server: %s\n", $this->mysqli->connect_error );
				exit();
			}
			
			// create database
			$sql = file_get_contents('sql/create_db.sql');
			$result = $this->query( $sql, array($conf["database"]) );
			if ( $result )
			{
				$this->mysqli->close();
				$this->mysqli = new mysqli( $conf["host"], $conf["user"], $conf["password"], $conf["database"] );
				
				self::setDbCharset();
				self::createTables();
			} 
			else 
			{
				printf( "Error creating database: %s\n", $this->mysqli->error );
				exit();
			}
		}
		
		/**
		 * Setup DB charset.
		 */
		private function setDbCharset()
		{
			if (!$this->mysqli->set_charset("utf8"))
			{
				printf("Can't set charset utf8: %s\n", $this->mysqli->error);
				exit();
			}
			else
			{
				//printf("Current charset: %s\n", $this->mysqli->character_set_name());
			}
		}
		
		/**
		 * Create new tables.
		 */
		private function createTables()
		{
			// create table
			$sql = file_get_contents( "sql/create_table.sql" );
			if ($this->mysqli->multi_query($sql) !== TRUE)
			{
				printf( "Error creating table: %s\n", $this->mysqli->error );
				exit;
			}
		}

		/**
		 * Perform query to DB.
		 * @param type $sql
		 * @param type $params
		 * @return type
		 */
		private function query( $sql, $params = array() )
		{
			$db = $this->mysqli;
			$sql = preg_replace_callback('/\!|\?|\#/', function ($matches) use ( &$db, &$params )
			{
				$param = array_shift($params);
				if ( $matches[0] == '!' )
				{
					$key = preg_replace( '/[^a-zA-Z0-9\-_=<> ]/', '', is_object($param)?$param->key:$param );
					return '"'.$key.'"';
				}
				else if ( $matches[0] == '#' )
				{
					$key = preg_replace( '/[^a-zA-Z0-9\-_=<> ]/', '', is_object($param)?$param->key:$param );
					return '`'.$key.'`';
				}
				else
				{
					if ( is_array($param) )
					{
						return '('.implode(',',array_map(function($v) use (&$db)
						{
							return "'".$db->real_escape_string($v)."'";
						}, $param)).')';
					}
					
					if ( is_object($param) && $param->type == 'hex' )
					{
						return "'".$db->real_escape_string($param->value)."'";
					}
					
					if ( is_object($param) && $param->type == 'wkt' )
					{
						return "'".$db->real_escape_string($param->value)."'";
					}
					
					if ( $param === null )
					{
						return 'NULL';
					}
					
					return "'".$db->real_escape_string($param)."'";
				}
			}, $sql );
			
			try
			{
				$result = $db->query($sql);
			} 
			catch ( \Exception $e )
			{ 
				$result = null;
			}
			
			return $result;
		}
		
		// -----------------------------
		//  REST API methods
		// -----------------------------

		/**
		 * Return API information.
		 */
		private function info()
		{
			$message = [
				"apiVersionMajor"	=> self::API_VERSION_MAJOR,
				"apiVersionMinor"	=> self::API_VERSION_MINOR,
				"phpVersion"		=> phpversion(),
				"mysqlVersion"		=> $this->mysqli ? $this->mysqli->server_info : "DB is disconnected"
			];

			$this->response( $message, 200 );
		}

		/**
		 * Track user.
		 */
		private function track()
		{
			// cross validation if the request method is POST else it will return "Not Acceptable" status
			if ( $this->get_request_method() != "POST" )
			{
				$this->response( ["error" => "Use POST method"], 406 );
			}
			
			$post = file_get_contents( "php://input" );
			$obj = json_decode( $post );

			// parse request properties
			$eventType = isset($obj->{"eventType"}) ? $obj->{"eventType"} : "?";
			$uuid = isset($obj->{"uuid"}) ? $obj->{"uuid"} : "?";
			$assessmentId = isset($obj->{"assessmentId"}) ? $obj->{"assessmentId"} : "?";
			$assessmentVersion = isset($obj->{"assessmentVersion"}) ?intval($obj->{"assessmentVersion"}) : -1;
			$pageId = isset($obj->{"pageId"}) ? $obj->{"pageId"} : "?";
			$pageVersion = isset($obj->{"pageVersion"}) ? intval($obj->{"pageVersion"}) : -1;
			$questionId = isset($obj->{"questionId"}) ? $obj->{"questionId"} : NULL;
			$questionVersion = isset($obj->{"questionVersion"}) ? intval($obj->{"questionVersion"}) : NULL;
			$optionId = isset($obj->{"optionId"}) ? $obj->{"optionId"} : NULL;
			$optionText = isset($obj->{"optionText"}) ? $obj->{"optionText"} : NULL;
			$optionComment = isset($obj->{"optionComment"}) ? $obj->{"optionComment"} : NULL;
			$language = isset($obj->{"language"}) ? $obj->{"language"} : "?";

			// add track record
			$sql = file_get_contents("sql/insert_tracking.sql");
			$result = $this->query( $sql, array(
				$eventType,
				$uuid,
				$assessmentId,
				$assessmentVersion,
				$pageId,
				$pageVersion,
				$questionId,
				$questionVersion,
				$optionId,
				$optionText,
				$optionComment,
				$language ));
			
			if ( !$result )
			{
				$this->response( ["error" => "Error in SQL request!\n".$this->mysqli->error."\n"], 500 );
			}

			// if success everythig is good send header as "OK" return param
			$this->response( [], 200 );
		}
	}

	// Initiiate Library
	$api = new API;
	$api->processApi();