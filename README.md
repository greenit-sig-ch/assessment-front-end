# README #

## Assessment application 1.0 ##

This is the repository for the second generation front-end assessment application of the Swiss Green IT Special Interest Group.

### How do I get set up? ###

* Install [Node.js and npm](https://nodejs.org/en/download/) if they are not already on your machine.
* Verify that you are running at least **node 8.x.x** and **npm 3.x.x** by running `node -v` and `npm -v` in a terminal/console window.
  Older versions produce errors, but newer versions are fine.
* This project was generated with [Angular CLI](https://github.com/angular/angular-cli). Install the [Angular CLI](https://github.com/angular/angular-cli) globally.
  `npm install -g @angular/cli`
  That's all. Project is ready to set up.
* Next execute `npm install`.
* Run `ng serve` for a dev server. Navigate to `http://localhost:4201/`. The app will automatically reload if you change any of the source files.
* Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
* Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Libraries used ###

* [Angular](https://angular.io/) _The application development framework._
* [ngx-bootstrap](https://github.com/valor-software/ngx-bootstrap) _Contains all core Bootstrap components powered by Angular._
* [ngx-md](https://github.com/dimpu/ngx-md) _Angular markdown component._
* [ts-md5](https://github.com/cotag/ts-md5) _A MD5 implementation for TypeScript._
* [js-yaml](https://github.com/nodeca/js-yaml) _This is an implementation of YAML, a human-friendly data serialization language._
* [ngx-toastr](https://github.com/scttcper/ngx-toastr) _Toasts component_

## Documentation ##

The documentation is work in progress. It can be found in [this repository's Wiki](https://bitbucket.org/greenit-sig-ch/assessment-front-end/wiki/). The *Getting started* section contains the information on how to install and run the application.

### Who do I talk to? ###

* [Beat Koch](mailto:beatkoch.@greenitplus.ch)
* [Lukas Meyer](mailto:lukas@ninux.ch)
