import {AssessmentApplication} from './app.po';

describe('Assessment Application', () => {
	let page: AssessmentApplication;

	beforeEach(() => {
		page = new AssessmentApplication();
	});

	it('Check main application container content', () => {
		page.navigateTo();
		expect(page.getPageNavigator().isPresent());
		expect(page.getPageComponent().isPresent());
	});
});
