import {browser, element, by} from 'protractor';

export class AssessmentApplication {
	navigateTo() {
		return browser.get('/');
	}

	getPageNavigator() {
		return element(by.css('app-page-navigator'));
	}

	getPageComponent() {
		return element(by.css('app-page'));
	}
}
